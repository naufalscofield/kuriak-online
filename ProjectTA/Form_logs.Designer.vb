﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_logs
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_logs))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txtmode = New System.Windows.Forms.TextBox()
        Me.txtaname = New System.Windows.Forms.TextBox()
        Me.userPass = New System.Windows.Forms.TextBox()
        Me.txtaccnt = New System.Windows.Forms.TextBox()
        Me.userID = New System.Windows.Forms.TextBox()
        Me.empID = New System.Windows.Forms.TextBox()
        Me.btnRegistration = New System.Windows.Forms.Button()
        Me.btnRecords = New System.Windows.Forms.Button()
        Me.btnLogs = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.userimage = New System.Windows.Forms.PictureBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.userimage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Silver
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.txtmode)
        Me.Panel1.Controls.Add(Me.txtaname)
        Me.Panel1.Controls.Add(Me.userPass)
        Me.Panel1.Controls.Add(Me.txtaccnt)
        Me.Panel1.Controls.Add(Me.userID)
        Me.Panel1.Controls.Add(Me.empID)
        Me.Panel1.Controls.Add(Me.btnRegistration)
        Me.Panel1.Controls.Add(Me.btnRecords)
        Me.Panel1.Controls.Add(Me.btnLogs)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 375)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(653, 64)
        Me.Panel1.TabIndex = 143
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.MediumOrchid
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.Teal
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Image = Global.ProjectTA.My.Resources.Resources.chart_32
        Me.Button1.Location = New System.Drawing.Point(402, 6)
        Me.Button1.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Button1.Size = New System.Drawing.Size(65, 56)
        Me.Button1.TabIndex = 151
        Me.Button1.Text = "CHART"
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Button1.UseVisualStyleBackColor = False
        '
        'txtmode
        '
        Me.txtmode.BackColor = System.Drawing.Color.Gray
        Me.txtmode.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtmode.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtmode.Location = New System.Drawing.Point(622, 5)
        Me.txtmode.Name = "txtmode"
        Me.txtmode.Size = New System.Drawing.Size(10, 20)
        Me.txtmode.TabIndex = 60
        Me.txtmode.Visible = False
        '
        'txtaname
        '
        Me.txtaname.BackColor = System.Drawing.Color.Gray
        Me.txtaname.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtaname.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtaname.Location = New System.Drawing.Point(611, 5)
        Me.txtaname.Name = "txtaname"
        Me.txtaname.Size = New System.Drawing.Size(10, 20)
        Me.txtaname.TabIndex = 59
        Me.txtaname.Visible = False
        '
        'userPass
        '
        Me.userPass.BackColor = System.Drawing.Color.Gray
        Me.userPass.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.userPass.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.userPass.Location = New System.Drawing.Point(600, 5)
        Me.userPass.Name = "userPass"
        Me.userPass.Size = New System.Drawing.Size(10, 20)
        Me.userPass.TabIndex = 39
        Me.userPass.Visible = False
        '
        'txtaccnt
        '
        Me.txtaccnt.BackColor = System.Drawing.Color.Gray
        Me.txtaccnt.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtaccnt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtaccnt.Location = New System.Drawing.Point(590, 5)
        Me.txtaccnt.Name = "txtaccnt"
        Me.txtaccnt.Size = New System.Drawing.Size(10, 20)
        Me.txtaccnt.TabIndex = 38
        Me.txtaccnt.Visible = False
        '
        'userID
        '
        Me.userID.BackColor = System.Drawing.Color.Gray
        Me.userID.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.userID.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.userID.Location = New System.Drawing.Point(580, 5)
        Me.userID.Name = "userID"
        Me.userID.Size = New System.Drawing.Size(10, 20)
        Me.userID.TabIndex = 37
        Me.userID.Visible = False
        '
        'empID
        '
        Me.empID.BackColor = System.Drawing.Color.Gray
        Me.empID.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.empID.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.empID.Location = New System.Drawing.Point(570, 5)
        Me.empID.Name = "empID"
        Me.empID.Size = New System.Drawing.Size(10, 20)
        Me.empID.TabIndex = 36
        Me.empID.Visible = False
        '
        'btnRegistration
        '
        Me.btnRegistration.BackColor = System.Drawing.Color.Gold
        Me.btnRegistration.FlatAppearance.BorderColor = System.Drawing.Color.Teal
        Me.btnRegistration.FlatAppearance.BorderSize = 0
        Me.btnRegistration.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnRegistration.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.btnRegistration.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRegistration.ForeColor = System.Drawing.Color.Black
        Me.btnRegistration.Image = CType(resources.GetObject("btnRegistration.Image"), System.Drawing.Image)
        Me.btnRegistration.Location = New System.Drawing.Point(204, 5)
        Me.btnRegistration.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnRegistration.Name = "btnRegistration"
        Me.btnRegistration.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnRegistration.Size = New System.Drawing.Size(65, 56)
        Me.btnRegistration.TabIndex = 19
        Me.btnRegistration.Text = "REGISTER"
        Me.btnRegistration.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnRegistration.UseVisualStyleBackColor = False
        '
        'btnRecords
        '
        Me.btnRecords.BackColor = System.Drawing.Color.Tomato
        Me.btnRecords.FlatAppearance.BorderColor = System.Drawing.Color.Teal
        Me.btnRecords.FlatAppearance.BorderSize = 0
        Me.btnRecords.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnRecords.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.btnRecords.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRecords.ForeColor = System.Drawing.Color.Black
        Me.btnRecords.Image = CType(resources.GetObject("btnRecords.Image"), System.Drawing.Image)
        Me.btnRecords.Location = New System.Drawing.Point(270, 5)
        Me.btnRecords.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnRecords.Name = "btnRecords"
        Me.btnRecords.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnRecords.Size = New System.Drawing.Size(65, 56)
        Me.btnRecords.TabIndex = 20
        Me.btnRecords.Text = "RECORDS"
        Me.btnRecords.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnRecords.UseVisualStyleBackColor = False
        '
        'btnLogs
        '
        Me.btnLogs.BackColor = System.Drawing.Color.SteelBlue
        Me.btnLogs.FlatAppearance.BorderColor = System.Drawing.Color.Teal
        Me.btnLogs.FlatAppearance.BorderSize = 0
        Me.btnLogs.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnLogs.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.btnLogs.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogs.ForeColor = System.Drawing.Color.Black
        Me.btnLogs.Image = CType(resources.GetObject("btnLogs.Image"), System.Drawing.Image)
        Me.btnLogs.Location = New System.Drawing.Point(336, 6)
        Me.btnLogs.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnLogs.Name = "btnLogs"
        Me.btnLogs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnLogs.Size = New System.Drawing.Size(65, 56)
        Me.btnLogs.TabIndex = 23
        Me.btnLogs.Text = "LOGS"
        Me.btnLogs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnLogs.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.PictureBox1)
        Me.Panel2.Controls.Add(Me.userimage)
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(653, 73)
        Me.Panel2.TabIndex = 142
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.ProjectTA.My.Resources.Resources.kuriak_logo
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(117, 79)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'userimage
        '
        Me.userimage.BackColor = System.Drawing.Color.MediumSlateBlue
        Me.userimage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.userimage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.userimage.Image = Global.ProjectTA.My.Resources.Resources.noimg
        Me.userimage.Location = New System.Drawing.Point(584, 9)
        Me.userimage.Name = "userimage"
        Me.userimage.Size = New System.Drawing.Size(48, 42)
        Me.userimage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.userimage.TabIndex = 35
        Me.userimage.TabStop = False
        '
        'Label18
        '
        Me.Label18.BackColor = System.Drawing.Color.Black
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(199, 81)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(178, 20)
        Me.Label18.TabIndex = 148
        Me.Label18.Text = "Date And Time"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label19
        '
        Me.Label19.BackColor = System.Drawing.Color.Black
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(378, 81)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(252, 20)
        Me.Label19.TabIndex = 146
        Me.Label19.Text = "Action"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.Black
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(23, 81)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(175, 20)
        Me.Label15.TabIndex = 150
        Me.Label15.Text = "Username"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToResizeColumns = False
        Me.DataGridView1.AllowUserToResizeRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.DataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.ColumnHeadersVisible = False
        Me.DataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.DataGridView1.GridColor = System.Drawing.Color.Gray
        Me.DataGridView1.Location = New System.Drawing.Point(23, 101)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.DataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(607, 266)
        Me.DataGridView1.TabIndex = 144
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(3, 14)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(42, 38)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 56
        Me.PictureBox2.TabStop = False
        '
        'Form_logs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(653, 439)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form_logs"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form_logs"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.userimage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtmode As System.Windows.Forms.TextBox
    Friend WithEvents txtaname As System.Windows.Forms.TextBox
    Friend WithEvents userPass As System.Windows.Forms.TextBox
    Friend WithEvents txtaccnt As System.Windows.Forms.TextBox
    Friend WithEvents userID As System.Windows.Forms.TextBox
    Friend WithEvents empID As System.Windows.Forms.TextBox
    Friend WithEvents btnRegistration As System.Windows.Forms.Button
    Friend WithEvents btnRecords As System.Windows.Forms.Button
    Friend WithEvents btnLogs As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents userimage As System.Windows.Forms.PictureBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
End Class
