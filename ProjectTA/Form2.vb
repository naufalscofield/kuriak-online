﻿Imports System.Data.Odbc
Public Class Form2
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    Sub koneksi()
        lokasiDB = "Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub

    Public Sub addDGV()

        With dgv
            'menghilangkan row header
            .RowHeadersVisible = False
            'menghilangkan colum header
            .ColumnHeadersVisible = False
            'agar ketika baris di datagrid diklik 1x melalui mouse atau kursor, satu baris penuh yang pilih, bukan per kolom
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Columns(0).Width = 100
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 140
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).Width = 90
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).Width = 145
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).Width = 220
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(5).Width = 220
            .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(6).Width = 30
            .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(7).Width = 80
            .Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(8).Width = 80
            .Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(9).Width = 30
            .Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(10).Width = 70
            .Columns(10).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(11).Width = 50
            .Columns(11).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(12).Width = 100
            .Columns(12).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(13).Width = 38
            .Columns(13).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter



        End With
    End Sub
    Sub refreshdata()
        Call koneksi()
        da = New OdbcDataAdapter("select * from `order` where status = 'BS' ", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "`order`")
        dgv.DataSource = (ds.Tables("`order`"))
    End Sub
 

    Private Sub Form2_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Call refreshdata()
        Call addDGV()

    End Sub
End Class