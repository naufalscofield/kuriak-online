﻿Imports System.Data.Odbc

Public Class FormKuli

    Public Sub addDGV()
        
        With DataGridView
            'menghilangkan row header
            .RowHeadersVisible = False
            'menghilangkan colum header
            .ColumnHeadersVisible = False
            'agar ketika baris di datagrid diklik 1x melalui mouse atau kursor, satu baris penuh yang pilih, bukan per kolom
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Columns(0).Width = 30
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 249
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).Width = 200
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).Width = 120
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).Width = 108
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        End With
    End Sub

    Private Sub dgvrefresh()

        Using conn As New OdbcConnection("Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306")
            conn.Open()
            Dim command As New OdbcCommand("SELECT * FROM kuli ", conn)
            Dim adapter As New OdbcDataAdapter
            Dim dt As New DataTable
            adapter.SelectCommand = command
            adapter.Fill(dt)
            DataGridView.DataSource = dt
            addDGV()
            adapter.Dispose()
            command.Dispose()
            conn.Close()
        End Using

    End Sub

    Private Sub FormKuli_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dgvrefresh()
        addDGV()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If nama.Text = "" Then
            MessageBox.Show("Insert Name Please!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        ElseIf kec.Text = "" Then
            MessageBox.Show("Insert Kecamatan Please!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        ElseIf kontak.Text = "" Then
            MessageBox.Show("Insert Contact Please!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            Try
                Using conn As New OdbcConnection("Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306")
                    conn.Open()
                   
                    Dim command As New OdbcCommand("insert into kuli (nama_kuli, kecamatan, no_telp) values ('" & nama.Text & "', '" & kec.Text & "', '" & kontak.Text & "')", conn)

                    '//// Logging action
                    Try
                        Using con2 As New OdbcConnection("Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306")
                            con2.Open()
                            Dim dt As New DateTime
                            dt = Now
                            Dim command3 As New OdbcCommand("INSERT INTO `logs` (`logdate`, `username`, `action`) VALUES ('" & dt.ToString & "', '" & UserID.Text & "', '" & UserID.Text + "registered a Kuli named: " + nama.Text & "')", con2)

                            command3.ExecuteNonQuery()
                            command3.Dispose()
                            con2.Close()
                        End Using
                    Catch ex As Exception
                        MessageBox.Show(ex.Message, "ERROR12", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try
                    '//// End Logging

                    command.ExecuteNonQuery()
                    MessageBox.Show("Kuli's Informations Successfuly Recorded!", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    command.Dispose()
                    conn.Close()
                    kec.Text = ""
                    kontak.Text = ""
                    nama.Text = ""
                    GroupBox1.Visible = False
                    dgvrefresh()
                End Using
            Catch ex As Exception
                MessageBox.Show(ex.Message, "ERROR12", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End If
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        GroupBox1.Visible = True
    End Sub

    Private Sub Panel3_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel3.Paint

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Try
            If DataGridView.Rows.Count > 0 Then
                If MessageBox.Show("Are you sure want to delete the record of Kuli " + DataGridView.CurrentRow.Cells(1).Value.ToString + "?", "CONFIRMATION", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = Windows.Forms.DialogResult.Yes Then
                    '//// Logging action
                    Try
                        Using con2 As New OdbcConnection("Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306")
                            con2.Open()
                            Dim dt As New DateTime
                            Dim dgvName As String

                            dgvName = DataGridView.CurrentRow.Cells(1).Value
                            dt = Now
                            Dim command3 As New OdbcCommand("INSERT INTO `logs` (`logdate`, `username`, `action`) VALUES ('" & dt.ToString & "', '" & UserID.Text & "', '" & UserID.Text + "Deleted the record of: " + dgvName & "')", con2)

                            With command3.Parameters
                                .AddWithValue("@logdate", dt.ToString)
                                .AddWithValue("@accnt", UserID.Text)
                                 End With
                            command3.ExecuteNonQuery()
                            command3.Dispose()
                            con2.Close()
                        End Using
                    Catch ex As Exception
                        MessageBox.Show(ex.Message, "ERROR12", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try
                    '//// End Logging
                    Using conn As New OdbcConnection("Dsn=DSNKuriak;uid=root;server=localhost;database=kuriak;port=3306")
                        conn.Open()
                        Dim command As New OdbcCommand("delete from kuli where id_kuli = '" & DataGridView.CurrentRow.Cells(0).Value.ToString & "' ", conn)
                        command.Parameters.AddWithValue("@emp", DataGridView.CurrentRow.Cells(0).Value)
                        command.ExecuteNonQuery()
                        command.Dispose()
                        Dim command2 As New OdbcCommand("select * from kuli", conn)
                        Dim adapter2 As New OdbcDataAdapter
                        Dim dt2 As New DataTable
                        adapter2.SelectCommand = command2
                        adapter2.Fill(dt2)
                        DataGridView.DataSource = dt2
                        addDGV()
                        adapter2.Dispose()
                        command2.Dispose()
                        conn.Close()

                    End Using
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "ERROR12", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        dgvrefresh()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            Using conn As New OdbcConnection("Dsn=DSNKuriak;uid=root;server=localhost;database=kuriak;port=3306")
                conn.Open()
                Dim query As String = "SELECT * FROM kuli "
                Dim where As String = ""
                If cb1.Checked Then where &= " status = 'Available' AND"
                If cb2.Checked Then where &= " status = 'Busy' AND"
               If where <> "" Then query &= "WHERE" & where
                query = query.Substring(0, Len(query) - 3)

                Dim cmd As New OdbcCommand(query, conn)
                Dim adapter As New OdbcDataAdapter
                Dim dt As New DataTable
                adapter.SelectCommand = cmd
                adapter.Fill(dt)
                DataGridView.DataSource = dt
                addDGV()
                adapter.Dispose()
                cmd.Dispose()
                conn.Close()
                dgvrefresh()
            End Using
        Catch
            MsgBox("Please check a data to filter!")
        End Try
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        With FormMenu
            .userID.Text = UserID.Text
            .Show()
            Me.Close()
        End With
    End Sub
End Class