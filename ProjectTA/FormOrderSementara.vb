﻿Imports System.Data.Odbc
Public Class FormOrderSementara
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    Sub koneksi()
        lokasiDB = "Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub
    
    Public Sub addDGV()

        With dgv
            'menghilangkan row header
            .RowHeadersVisible = False
            'menghilangkan colum header
            .ColumnHeadersVisible = False
            'agar ketika baris di datagrid diklik 1x melalui mouse atau kursor, satu baris penuh yang pilih, bukan per kolom
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Columns(0).Width = 30
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 140
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).Width = 90
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).Width = 145
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).Width = 220
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(5).Width = 220
            .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(6).Width = 30
            .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(7).Width = 80
            .Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(8).Width = 80
            .Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(9).Width = 30
            .Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(10).Width = 70
            .Columns(10).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(11).Width = 50
            .Columns(11).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(12).Width = 100
            .Columns(12).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(13).Width = 38
            .Columns(13).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter



        End With
    End Sub
    Sub refreshdata()
        Call koneksi()
        da = New OdbcDataAdapter("select * from `order` where status = 'BS' ", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "`order`")
        dgv.DataSource = (ds.Tables("`order`"))
    End Sub
    Sub refreshdata2()
        Call koneksi()
        da = New OdbcDataAdapter("select * from `kuli` where status = 'Y' ", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "`kuli`")
        dgv2.DataSource = (ds.Tables("`kuli`"))
    End Sub

    Private Sub FormOrderSementara_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call refreshdata()
        Call refreshdata2()
        addDGV()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        GroupBox2.Visible = False
        dgv.Visible = True
        PictureBox2.Visible = True
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Call koneksi()
        Dim id As String = Label31.Text
        Dim edit As String = "UPDATE order SET status ='" & TextBox1.Text & "' where id_order = '" & id & "'"
        cmd = New OdbcCommand(edit, conn)
        cmd.ExecuteNonQuery()
        MsgBox("Data Berhasil di Ubah!", MsgBoxStyle.Information, "Information")
        GroupBox2.Visible = False
        dgv2.Visible = True
        Panel1.Visible = True
        Call refreshdata2()
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    End Sub

    

    Private Sub dgv_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellContentDoubleClick
        PictureBox2.Visible = False
        dgv.Visible = False
        GroupBox2.Visible = True
        Dim a As Object = dgv.Rows(e.RowIndex).Cells(0).Value
        Dim b As Object = dgv.Rows(e.RowIndex).Cells(1).Value
        Dim l As Object = dgv.Rows(e.RowIndex).Cells(11).Value
        Dim c As Object = dgv.Rows(e.RowIndex).Cells(6).Value
        Label30.Text = b
        Label31.Text = a
        Label32.Text = l
        Label34.Text = c
    End Sub

    Private Sub TextBox9_TextChanged(sender As System.Object, e As System.EventArgs) Handles TextBox9.TextChanged

    End Sub

    Private Sub dgv_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellContentClick

    End Sub
End Class