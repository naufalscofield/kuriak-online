﻿Imports System.Data.Odbc
Imports System.Drawing.Imaging
Public Class Form_record

    Inherits System.Windows.Forms.Form
    Dim bytImage() As Byte

    Public Sub addDGV()
        With DataGridView
            .Columns(0).Width = 125
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 157
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .Columns(2).Width = 130
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .Columns(3).Width = 107
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            End With
    End Sub

    Private Sub dgvrefresh()

        Using conn As New OdbcConnection("Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306")
            conn.Open()
            Dim command As New OdbcCommand("SELECT username, nama_lengkap, email, no_telp FROM users WHERE level = 'Karyawan' ", conn)
            Dim adapter As New OdbcDataAdapter
            Dim dt As New DataTable
            adapter.SelectCommand = command
            adapter.Fill(dt)
            DataGridView.DataSource = dt
            addDGV()
            adapter.Dispose()
            command.Dispose()
            conn.Close()
        End Using

    End Sub

    Private Sub ShowAdminPhoto()
        Try

            Dim cmd As Odbc.OdbcCommand
            Dim dr As Odbc.OdbcDataReader
            Dim eID As String

            Using cn As New OdbcConnection("Dsn=DSNKuriak;uid=root;server=localhost;database=kuriak;port=3306")
                cn.Open()

                eID = userID.Text
                cmd = cn.CreateCommand()
                cmd.CommandText = "SELECT image FROM users WHERE username = '" & userID.Text & "' "

                dr = cmd.ExecuteReader

                If dr.Read Then

                    Try
                        bytImage = CType(dr(0), Byte())
                        Dim ms As New System.IO.MemoryStream(bytImage)
                        Dim bmImage As New Bitmap(ms)
                        ms.Close()

                        userimage.Image = bmImage
                        userimage.Refresh()
                    Catch ex As Exception
                        MsgBox(ex.ToString)
                    End Try
                End If

                dr.Close()
                cn.Close()

                cmd.Dispose()
                cn.Dispose()
            End Using
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub ShowDetails2()
        Try

            Dim cmd As Odbc.OdbcCommand
            Dim dr As Odbc.OdbcDataReader

            Using cn As New OdbcConnection("Dsn=DSNKuriak;uid=root;server=localhost;database=kuriak;port=3306")
                cn.Open()

                cmd = cn.CreateCommand()
                cmd.CommandText = "SELECT image FROM users WHERE username = '" & DataGridView.CurrentRow.Cells(0).Value & "' "

                dr = cmd.ExecuteReader

                If dr.Read Then

                    Try
                        bytImage = CType(dr(0), Byte())
                        Dim ms As New System.IO.MemoryStream(bytImage)
                        Dim bmImage As New Bitmap(ms)
                        ms.Close()

                        myimage2.Image = bmImage
                        myimage2.Refresh()
                    Catch ex As Exception
                        MsgBox(ex.ToString)
                    End Try
                End If

                dr.Close()
                cn.Close()

                cmd.Dispose()
                cn.Dispose()
            End Using
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub Form_record_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ShowAdminPhoto()
        Me.CenterToScreen()

        dgvrefresh()

        If DataGridView.Rows.Count > 0 Then
            DataGridView.CurrentRow.Selected = False
        Else
            MsgBox("Record was empty!")
        End If

        btnEdit.Enabled = False
        btnDelete.Enabled = False
        btnProfile.Enabled = False

    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        dgvrefresh()

        If DataGridView.Rows.Count > 0 Then
            DataGridView.CurrentRow.Selected = False
        Else
            MsgBox("Record was empty!")
        End If

        myimage2.Image = ProjectTA.My.Resources.noimg
        lblPreview.Text = "Preview"

        btnEdit.Enabled = False
        btnDelete.Enabled = False
        btnProfile.Enabled = False

        txtSearch.Text = "  Enter Keyword To Search"
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If DataGridView.Rows.Count > 0 Then
                If MessageBox.Show("Are you sure want to delete the record of employee " + DataGridView.CurrentRow.Cells(1).Value.ToString + " with employee username " + DataGridView.CurrentRow.Cells(0).Value.ToString + "?", "CONFIRMATION", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = Windows.Forms.DialogResult.Yes Then
                    '//// Logging action
                    Try
                        Using con2 As New OdbcConnection("Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306")
                            con2.Open()
                            Dim dt As New DateTime
                            Dim dgvName As String

                            dgvName = DataGridView.CurrentRow.Cells(1).Value
                            dt = Now
                            Dim command3 As New OdbcCommand("INSERT INTO `logs` (`logdate`, `username`, `action`) VALUES ('" & dt.ToString & "', '" & userID.Text & "', '" & userID.Text + "Deleted the record of: " + dgvName & "')", con2)

                            With command3.Parameters
                                .AddWithValue("@logdate", dt.ToString)
                                .AddWithValue("@accnt", userID.Text)
                                .AddWithValue("@aname", txtaname.Text)
                                .AddWithValue("@action", txtmode.Text & " " & txtaname.Text & " " & "Deleted the record of: " & dgvName)
                            End With
                            command3.ExecuteNonQuery()
                            command3.Dispose()
                            con2.Close()
                        End Using
                    Catch ex As Exception
                        MessageBox.Show(ex.Message, "ERROR12", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try
                    '//// End Logging
                    Using conn As New OdbcConnection("Dsn=DSNKuriak;uid=root;server=localhost;database=kuriak;port=3306")
                        conn.Open()
                        Dim command As New OdbcCommand("delete from users where username = '" & DataGridView.CurrentRow.Cells(0).Value.ToString & "' ", conn)
                        command.Parameters.AddWithValue("@emp", DataGridView.CurrentRow.Cells(0).Value)
                        command.ExecuteNonQuery()
                        command.Dispose()
                        Dim command2 As New OdbcCommand("select * from users", conn)
                        Dim adapter2 As New OdbcDataAdapter
                        Dim dt2 As New DataTable
                        adapter2.SelectCommand = command2
                        adapter2.Fill(dt2)
                        DataGridView.DataSource = dt2
                        addDGV()
                        adapter2.Dispose()
                        command2.Dispose()
                        conn.Close()
                        myimage2.Image = ProjectTA.My.Resources.noimg
                        lblPreview.Text = "Preview"
                    End Using
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "ERROR12", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        Me.btnRefresh.PerformClick()
    End Sub

    Private Sub btnProfile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProfile.Click
            Try
                If DataGridView.Rows.Count > 0 Then
                    With Form_profile
                        .lbluser.Text = DataGridView.CurrentRow.Cells(0).Value.ToString
                        .lblname.Text = DataGridView.CurrentRow.Cells(1).Value.ToString
                    .lblemail.Text = DataGridView.CurrentRow.Cells(2).Value.ToString
                    .lblnohp.Text = DataGridView.CurrentRow.Cells(3).Value.ToString
                    .myimage.Image = myimage2.Image
                        .ShowDialog()
                    End With
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, "ERROR11", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
    End Sub

    Private Sub txtSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.Click
        txtSearch.Text = ""
    End Sub

    Private Sub txtSearch_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSearch.KeyPress
        Try
            Using conn As New OdbcConnection("Dsn=DSNKuriak;uid=root;server=localhost;database=kuriak;port=3306")
                conn.Open()
                Dim command As New OdbcCommand("select * from users where (username like '" & txtSearch.Text & "') or (nama_lengkap like '%' + '" & txtSearch.Text & "' + '%') or (email like '%' + '" & txtSearch.Text & "' + '%') or (no_telp like '%' + '" & txtSearch.Text & "' + '%')", conn)
                With command.Parameters
                    .AddWithValue("@id", txtSearch.Text)
                    .AddWithValue("@fname", txtSearch.Text)
                    .AddWithValue("@lname", txtSearch.Text)
                    .AddWithValue("@dept", txtSearch.Text)
                    .AddWithValue("@pos", txtSearch.Text)
                End With
                Dim adapter As New OdbcDataAdapter
                Dim dt As New DataTable
                adapter.SelectCommand = command
                adapter.Fill(dt)
                DataGridView.DataSource = dt
                addDGV()
                adapter.Dispose()
                command.Dispose()
                conn.Close()
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message, "ERROR4", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub abox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub kbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
       
    End Sub

    Private Sub DataGridView_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView.CellClick
        ShowDetails2()
        username.Text = DataGridView.CurrentRow.Cells(0).Value
        lblPreview.Text = DataGridView.CurrentRow.Cells(1).Value
        btnEdit.Enabled = True
        btnDelete.Enabled = True
        btnProfile.Enabled = True

    End Sub

    Private Sub btnRegistration_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistration.Click
        With Form_reg
            .userID.Text = userID.Text
            .txtaccnt.Text = txtaccnt.Text
            .userPass.Text = userPass.Text
            .txtaname.Text = txtaname.Text
            .txtmode.Text = txtmode.Text
            .Show()
        End With
        Me.Close()
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
                With Form_reg

                    .select_image.Text = "Change Photo"
                    .btnRegister.Text = "Update"
                    .btnReset.Text = "Cancel"
                    .userID.Text = userID.Text
                    .txtaccnt.Text = txtaccnt.Text
                    .userPass.Text = userPass.Text

                    If DataGridView.Rows.Count > 0 Then

                    .username.Text = DataGridView.CurrentRow.Cells(0).Value.ToString
                    .nama.Text = DataGridView.CurrentRow.Cells(1).Value.ToString
                    .email.Text = DataGridView.CurrentRow.Cells(2).Value.ToString
                    .pn.Text = DataGridView.CurrentRow.Cells(3).Value.ToString
                        .txtmode.Text = txtmode.Text
                        .txtaname.Text = txtaname.Text
                        .userID.Text = userID.Text
                    .username.Enabled = False
                        .Show()

                    End If
                End With
                Me.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "ERROR11", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub btnLogs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogs.Click
        With Form_logs
            .userID.Text = userID.Text
            .Show()
        End With
        Me.Close()
    End Sub

    Private Sub aname_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Using con2 As New OdbcConnection("Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306")
                con2.Open()
                Dim dt As New DateTime
                dt = Now
                Dim command3 As New OdbcCommand("INSERT INTO `logs` (`logdate`, `username`, `action`) VALUES ('" & dt.ToString & "', '" & userID.Text & "','" & userID.Text + " was logged out." & "')", con2)

                With command3.Parameters
                    .AddWithValue("@logdate", dt.ToString)
                    .AddWithValue("@user", userID.Text)
                    .AddWithValue("@action", txtmode.Text & " " & txtaname.Text & " was logged out.")
                End With
                command3.ExecuteNonQuery()
                command3.Dispose()
                con2.Close()
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message, "ERROR12", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Me.Close()
        Form_reg.Close()
        Form_logs.Close()
        Form_chart.Close()
        FormLogin.Show()
    End Sub

    Private Sub btnRecords_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRecords.Click

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        With Form_chart
            .userID.Text = userID.Text
            .txtaccnt.Text = txtaccnt.Text
            .userPass.Text = userPass.Text
            .txtaname.Text = txtaname.Text
            .txtmode.Text = txtmode.Text
            .Show()
        End With
        Me.Close()
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        With FormMenu
            .userID.Text = userID.Text
            .Show()
            Me.Close()
        End With
    End Sub
End Class