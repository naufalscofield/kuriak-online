﻿Imports System.Data.Odbc
Public Class FormHarga
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    Sub koneksi()
        lokasiDB = "Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub
    Private Sub FormHarga_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call koneksi()
        cmd = New OdbcCommand("select harga_kuli from setting", conn)
        rd = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows() Then
            Label2.Text = rd.Item("harga_kuli")
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GroupBox2.Visible = True
        Button1.Enabled = False
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        TextBox1.Text = ""
        GroupBox2.Visible = False
        Button1.Enabled = True
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If TextBox1.Text = "" Then
            MsgBox("Harap Masukan Tarif yang akan di revisi!")

        Else


            Dim Pesan As String = MsgBox("Tarif Kuli akan di revisi,yakin?", vbYesNo)
            If Pesan = vbYes Then
                Call koneksi()
                Dim simpan As String = "UPDATE setting SET harga_kuli='" & TextBox1.Text & "'"
                cmd = New OdbcCommand(simpan, conn)
                cmd.ExecuteNonQuery()
                MsgBox("Tarif Sudah Di Revisi", MsgBoxStyle.Information, "Kuriak Online")
                Call koneksi()
                cmd = New OdbcCommand("select harga_kuli from setting", conn)
                rd = cmd.ExecuteReader
                rd.Read()
                If rd.HasRows() Then
                    Label2.Text = rd.Item("harga_kuli")
                End If
                TextBox1.Text = ""
                GroupBox1.Visible = True
                Button1.Enabled = True
                GroupBox2.Visible = False

                Call koneksi()
                cmd = New OdbcCommand("select harga_kuli from setting", conn)
                rd = cmd.ExecuteReader
                rd.Read()
                If rd.HasRows() Then
                    Label2.Text = rd.Item("harga_kuli")
                End If
            Else
            End If


        End If
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim strValid As String = "0123456789-.,"

        Dim x As Long = InStr(strValid, e.KeyChar)

        If x = 0 And Asc(e.KeyChar) <> 8 And Asc(e.KeyChar) <> 32 Then

            e.KeyChar = ""

        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        With FormMenu
            .userID.Text = UserID.Text
            .Show()
            Me.Close()
        End With
    End Sub
End Class