﻿Imports System.Data.Odbc
Public Class FormLihat
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    Sub koneksi()
        lokasiDB = "Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub
    Public Sub addDGV()

        With dgv
            'menghilangkan row header
            .RowHeadersVisible = False
            'menghilangkan colum header
            .ColumnHeadersVisible = False
            'agar ketika baris di datagrid diklik 1x melalui mouse atau kursor, satu baris penuh yang pilih, bukan per kolom
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Columns(0).Width = 30
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 140
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).Width = 90
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).Width = 145
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).Width = 220
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(5).Width = 220
            .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(6).Width = 30
            .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(7).Width = 80
            .Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(8).Width = 80
            .Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(9).Width = 30
            .Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(10).Width = 70
            .Columns(10).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(11).Width = 50
            .Columns(11).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(12).Width = 100
            .Columns(12).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(13).Width = 38
            .Columns(13).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter



        End With
    End Sub

    Sub refreshdata()
        Call koneksi()
        da = New OdbcDataAdapter("select * from `order`", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "`order`")
        dgv.DataSource = (ds.Tables("`order`"))
    End Sub
    Private Sub FormLihat_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call refreshdata()
        addDGV()
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Me.Hide()
        FormPrint.Show()
        FormPrint.ComboBox1.Text = Me.TextBox1.Text
    End Sub

    Private Sub dgv_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellClick
        
    End Sub

    Private Sub dgv_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellContentClick
        
    End Sub

    Private Sub TextBox9_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox9.Click
        TextBox9.Text = ""
    End Sub

    Private Sub TextBox9_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox9.GotFocus
        TextBox9.Text = "Temukan"
    End Sub


    Private Sub TextBox9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox9.TextChanged
        Call koneksi()
        cmd = New OdbcCommand("select * from `order` where pengorder like'%" & TextBox9.Text & "%'", conn)
        rd = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            da = New OdbcDataAdapter("select * from `order` where pengorder like '%" & TextBox9.Text & "%'", conn)
            ds = New DataSet
            ds.Clear()
            da.Fill(ds, "`order`")
            dgv.DataSource = ds.Tables("`order`")
        End If
    End Sub

    Private Sub dgv_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellContentDoubleClick
        dgv.BackgroundColor = Color.DarkGray
        GroupBox1.Visible = True
        Dim a As Object = dgv.Rows(e.RowIndex).Cells(0).Value
        Dim b As Object = dgv.Rows(e.RowIndex).Cells(1).Value
        Dim c As Object = dgv.Rows(e.RowIndex).Cells(2).Value
        Dim d As Object = dgv.Rows(e.RowIndex).Cells(3).Value
        Dim ep As Object = dgv.Rows(e.RowIndex).Cells(4).Value
        Dim f As Object = dgv.Rows(e.RowIndex).Cells(5).Value
        Dim g As Object = dgv.Rows(e.RowIndex).Cells(6).Value
        Dim h As Object = dgv.Rows(e.RowIndex).Cells(7).Value
        Dim i As Object = dgv.Rows(e.RowIndex).Cells(8).Value
        Dim j As Object = dgv.Rows(e.RowIndex).Cells(9).Value
        Dim k As Object = dgv.Rows(e.RowIndex).Cells(10).Value
        Dim l As Object = dgv.Rows(e.RowIndex).Cells(11).Value
        Dim m As Object = dgv.Rows(e.RowIndex).Cells(12).Value
        Dim n As Object = dgv.Rows(e.RowIndex).Cells(13).Value

        TextBox2.Text = a
        TextBox3.Text = b
        TextBox4.Text = c
        TextBox5.Text = d
        RichTextBox1.Text = ep
        RichTextBox2.Text = f
        TextBox6.Text = g
        TextBox7.Text = h
        TextBox8.Text = i
        TextBox10.Text = j
        TextBox11.Text = k
        TextBox12.Text = l
        TextBox13.Text = m
        TextBox14.Text = n
        If TextBox14.Text = "BS" Then
            Button1.Text = "Selesaikan Transfer!"
        End If
        If TextBox14.Text = "S" Then
            Button1.Text = "Transfer Selesai"
            Button1.Enabled = False
        End If
    End Sub

    Private Sub dgv_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgv.CellMouseClick
        Dim a As Object = dgv.Rows(e.RowIndex).Cells(0).Value

        TextBox1.Text = a
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If MessageBox.Show("Hapus Data Orderan Dengan ID Order " & dgv.Item(0, dgv.CurrentRow.Index).Value & _
           " ?", "KONFIRM", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = _
            Windows.Forms.DialogResult.Yes Then
            If dgv.CurrentRow.Index <> dgv.NewRowIndex Then
                Dim HapusDetail As String = "delete from `order` where id_order='" & _
    dgv.Item(0, dgv.CurrentRow.Index).Value & "'"
                cmd = New OdbcCommand(HapusDetail, conn)
                cmd.ExecuteNonQuery()

                dgv.Rows.RemoveAt(dgv.CurrentRow.Index)
            End If
        Else

        End If
        Call refreshdata()
    End Sub

    Private Sub dgv_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.DoubleClick
       
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Call koneksi()
        Dim id As String = dgv.SelectedCells(0).Value
        Dim edit As String = "UPDATE order SET status = '" & TextBox14.Text & "' where idorder = '" & id & "'"
        cmd = New OdbcCommand(edit, conn)
        cmd.ExecuteNonQuery()
        Call refreshdata()
        Button1.Text = "Transfer Selesai!"
        TextBox14.Text = "S"
        GroupBox1.Visible = False
    End Sub

    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        GroupBox1.Visible = False
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            Using conn As New OdbcConnection("Dsn=DSNKuriak;uid=root;server=localhost;database=kuriak;port=3306")
                conn.Open()
                Dim query As String = "SELECT * FROM order "
                Dim where As String = ""
                If cb1.Checked Then where &= " status = 'S' AND"
                If cb2.Checked Then where &= " status = 'BS' AND"
                If where <> "" Then query &= "WHERE" & where
                query = query.Substring(0, Len(query) - 3)

                Dim cmd As New OdbcCommand(query, conn)
                Dim adapter As New OdbcDataAdapter
                Dim dt As New DataTable
                adapter.SelectCommand = cmd
                adapter.Fill(dt)
                dgv.DataSource = dt
                adapter.Dispose()
                cmd.Dispose()
                conn.Close()
                refreshdata()
                addDGV()
            End Using
        Catch
            MsgBox("Please check a data to filter!")
        End Try
    End Sub
End Class