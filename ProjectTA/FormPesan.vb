﻿Imports System.Data.Odbc
Public Class FormPesan
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    Sub koneksi()
        lokasiDB = "Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub
    Public Sub addDGV()

        With dgv
            'menghilangkan row header
            .RowHeadersVisible = False
            'menghilangkan colum header
            .ColumnHeadersVisible = False
            'agar ketika baris di datagrid diklik 1x melalui mouse atau kursor, satu baris penuh yang pilih, bukan per kolom
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Columns(0).Width = 30
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 140
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).Width = 90
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).Width = 145
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
          



        End With
    End Sub

    Sub refreshdata()
        Call koneksi()
        da = New OdbcDataAdapter("select * from pesan", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "pesan")
        dgv.DataSource = (ds.Tables("pesan"))
    End Sub

    Private Sub FormPesan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call koneksi()
        Call refreshdata()
        Call addDGV()
    End Sub

    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click

    End Sub

    Private Sub Label41_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label41.Click

    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        With FormMenu
            .Show()
            Me.Close()
        End With
    End Sub
End Class