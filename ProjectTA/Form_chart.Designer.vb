﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_chart
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_chart))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.userimage = New System.Windows.Forms.PictureBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txtmode = New System.Windows.Forms.TextBox()
        Me.txtaname = New System.Windows.Forms.TextBox()
        Me.userPass = New System.Windows.Forms.TextBox()
        Me.txtaccnt = New System.Windows.Forms.TextBox()
        Me.userID = New System.Windows.Forms.TextBox()
        Me.empID = New System.Windows.Forms.TextBox()
        Me.btnRegistration = New System.Windows.Forms.Button()
        Me.btnRecords = New System.Windows.Forms.Button()
        Me.btnLogs = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_income = New System.Windows.Forms.TextBox()
        Me.txt_expend = New System.Windows.Forms.TextBox()
        Me.DTP = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Chart1 = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.userimage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.userimage)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(727, 73)
        Me.Panel1.TabIndex = 1
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.ProjectTA.My.Resources.Resources.kuriak_logo
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(117, 79)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'userimage
        '
        Me.userimage.BackColor = System.Drawing.Color.MediumSlateBlue
        Me.userimage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.userimage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.userimage.Image = Global.ProjectTA.My.Resources.Resources.noimg
        Me.userimage.Location = New System.Drawing.Point(665, 12)
        Me.userimage.Name = "userimage"
        Me.userimage.Size = New System.Drawing.Size(48, 42)
        Me.userimage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.userimage.TabIndex = 35
        Me.userimage.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Panel2.Controls.Add(Me.PictureBox2)
        Me.Panel2.Controls.Add(Me.Button1)
        Me.Panel2.Controls.Add(Me.txtmode)
        Me.Panel2.Controls.Add(Me.txtaname)
        Me.Panel2.Controls.Add(Me.userPass)
        Me.Panel2.Controls.Add(Me.txtaccnt)
        Me.Panel2.Controls.Add(Me.userID)
        Me.Panel2.Controls.Add(Me.empID)
        Me.Panel2.Controls.Add(Me.btnRegistration)
        Me.Panel2.Controls.Add(Me.btnRecords)
        Me.Panel2.Controls.Add(Me.btnLogs)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 365)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(727, 64)
        Me.Panel2.TabIndex = 144
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.MediumOrchid
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.Teal
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Image = Global.ProjectTA.My.Resources.Resources.chart_32
        Me.Button1.Location = New System.Drawing.Point(433, 6)
        Me.Button1.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Button1.Size = New System.Drawing.Size(65, 56)
        Me.Button1.TabIndex = 151
        Me.Button1.Text = "CHART"
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Button1.UseVisualStyleBackColor = False
        '
        'txtmode
        '
        Me.txtmode.BackColor = System.Drawing.Color.Gray
        Me.txtmode.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtmode.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtmode.Location = New System.Drawing.Point(622, 5)
        Me.txtmode.Name = "txtmode"
        Me.txtmode.Size = New System.Drawing.Size(10, 20)
        Me.txtmode.TabIndex = 60
        Me.txtmode.Visible = False
        '
        'txtaname
        '
        Me.txtaname.BackColor = System.Drawing.Color.Gray
        Me.txtaname.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtaname.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtaname.Location = New System.Drawing.Point(611, 5)
        Me.txtaname.Name = "txtaname"
        Me.txtaname.Size = New System.Drawing.Size(10, 20)
        Me.txtaname.TabIndex = 59
        Me.txtaname.Visible = False
        '
        'userPass
        '
        Me.userPass.BackColor = System.Drawing.Color.Gray
        Me.userPass.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.userPass.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.userPass.Location = New System.Drawing.Point(600, 5)
        Me.userPass.Name = "userPass"
        Me.userPass.Size = New System.Drawing.Size(10, 20)
        Me.userPass.TabIndex = 39
        Me.userPass.Visible = False
        '
        'txtaccnt
        '
        Me.txtaccnt.BackColor = System.Drawing.Color.Gray
        Me.txtaccnt.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtaccnt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtaccnt.Location = New System.Drawing.Point(590, 5)
        Me.txtaccnt.Name = "txtaccnt"
        Me.txtaccnt.Size = New System.Drawing.Size(10, 20)
        Me.txtaccnt.TabIndex = 38
        Me.txtaccnt.Visible = False
        '
        'userID
        '
        Me.userID.BackColor = System.Drawing.Color.Gray
        Me.userID.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.userID.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.userID.Location = New System.Drawing.Point(580, 5)
        Me.userID.Name = "userID"
        Me.userID.Size = New System.Drawing.Size(10, 20)
        Me.userID.TabIndex = 37
        Me.userID.Visible = False
        '
        'empID
        '
        Me.empID.BackColor = System.Drawing.Color.Gray
        Me.empID.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.empID.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.empID.Location = New System.Drawing.Point(570, 5)
        Me.empID.Name = "empID"
        Me.empID.Size = New System.Drawing.Size(10, 20)
        Me.empID.TabIndex = 36
        Me.empID.Visible = False
        '
        'btnRegistration
        '
        Me.btnRegistration.BackColor = System.Drawing.Color.Gold
        Me.btnRegistration.FlatAppearance.BorderColor = System.Drawing.Color.Teal
        Me.btnRegistration.FlatAppearance.BorderSize = 0
        Me.btnRegistration.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnRegistration.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.btnRegistration.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRegistration.ForeColor = System.Drawing.Color.Black
        Me.btnRegistration.Image = CType(resources.GetObject("btnRegistration.Image"), System.Drawing.Image)
        Me.btnRegistration.Location = New System.Drawing.Point(235, 5)
        Me.btnRegistration.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnRegistration.Name = "btnRegistration"
        Me.btnRegistration.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnRegistration.Size = New System.Drawing.Size(65, 56)
        Me.btnRegistration.TabIndex = 19
        Me.btnRegistration.Text = "REGISTER"
        Me.btnRegistration.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnRegistration.UseVisualStyleBackColor = False
        '
        'btnRecords
        '
        Me.btnRecords.BackColor = System.Drawing.Color.Tomato
        Me.btnRecords.FlatAppearance.BorderColor = System.Drawing.Color.Teal
        Me.btnRecords.FlatAppearance.BorderSize = 0
        Me.btnRecords.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnRecords.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.btnRecords.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRecords.ForeColor = System.Drawing.Color.Black
        Me.btnRecords.Image = CType(resources.GetObject("btnRecords.Image"), System.Drawing.Image)
        Me.btnRecords.Location = New System.Drawing.Point(301, 5)
        Me.btnRecords.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnRecords.Name = "btnRecords"
        Me.btnRecords.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnRecords.Size = New System.Drawing.Size(65, 56)
        Me.btnRecords.TabIndex = 20
        Me.btnRecords.Text = "RECORDS"
        Me.btnRecords.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnRecords.UseVisualStyleBackColor = False
        '
        'btnLogs
        '
        Me.btnLogs.BackColor = System.Drawing.Color.SteelBlue
        Me.btnLogs.FlatAppearance.BorderColor = System.Drawing.Color.Teal
        Me.btnLogs.FlatAppearance.BorderSize = 0
        Me.btnLogs.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnLogs.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.btnLogs.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogs.ForeColor = System.Drawing.Color.Black
        Me.btnLogs.Image = CType(resources.GetObject("btnLogs.Image"), System.Drawing.Image)
        Me.btnLogs.Location = New System.Drawing.Point(367, 6)
        Me.btnLogs.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnLogs.Name = "btnLogs"
        Me.btnLogs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnLogs.Size = New System.Drawing.Size(65, 56)
        Me.btnLogs.TabIndex = 23
        Me.btnLogs.Text = "LOGS"
        Me.btnLogs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnLogs.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txt_income)
        Me.GroupBox1.Controls.Add(Me.txt_expend)
        Me.GroupBox1.Controls.Add(Me.DTP)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.ForeColor = System.Drawing.Color.DimGray
        Me.GroupBox1.Location = New System.Drawing.Point(499, 101)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(214, 259)
        Me.GroupBox1.TabIndex = 146
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Income - Expenditure"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.ForeColor = System.Drawing.Color.DimGray
        Me.Label5.Location = New System.Drawing.Point(34, 56)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(57, 13)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Input Date"
        '
        'txt_income
        '
        Me.txt_income.Location = New System.Drawing.Point(37, 112)
        Me.txt_income.Name = "txt_income"
        Me.txt_income.Size = New System.Drawing.Size(119, 20)
        Me.txt_income.TabIndex = 21
        '
        'txt_expend
        '
        Me.txt_expend.Location = New System.Drawing.Point(37, 155)
        Me.txt_expend.Name = "txt_expend"
        Me.txt_expend.Size = New System.Drawing.Size(119, 20)
        Me.txt_expend.TabIndex = 24
        '
        'DTP
        '
        Me.DTP.Enabled = False
        Me.DTP.Location = New System.Drawing.Point(6, 72)
        Me.DTP.Name = "DTP"
        Me.DTP.Size = New System.Drawing.Size(200, 20)
        Me.DTP.TabIndex = 23
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.ForeColor = System.Drawing.Color.DimGray
        Me.Label6.Location = New System.Drawing.Point(37, 139)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(63, 13)
        Me.Label6.TabIndex = 25
        Me.Label6.Text = "Expenditure"
        '
        'Button2
        '
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(37, 181)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(119, 23)
        Me.Button2.TabIndex = 22
        Me.Button2.Text = "Insert"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.ForeColor = System.Drawing.Color.DimGray
        Me.Label2.Location = New System.Drawing.Point(37, 96)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 13)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "Income"
        '
        'Chart1
        '
        ChartArea1.Name = "ChartArea1"
        Me.Chart1.ChartAreas.Add(ChartArea1)
        Legend1.Name = "Legend1"
        Me.Chart1.Legends.Add(Legend1)
        Me.Chart1.Location = New System.Drawing.Point(12, 85)
        Me.Chart1.Name = "Chart1"
        Series1.ChartArea = "ChartArea1"
        Series1.Legend = "Legend1"
        Series1.Name = "Pemasukan"
        Series2.ChartArea = "ChartArea1"
        Series2.Legend = "Legend1"
        Series2.Name = "Pengeluaran"
        Me.Chart1.Series.Add(Series1)
        Me.Chart1.Series.Add(Series2)
        Me.Chart1.Size = New System.Drawing.Size(481, 274)
        Me.Chart1.TabIndex = 147
        Me.Chart1.Text = "Chart1"
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(3, 14)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(42, 38)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 55
        Me.PictureBox2.TabStop = False
        '
        'Form_chart
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ProjectTA.My.Resources.Resources.KURIAK_BG
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(727, 429)
        Me.Controls.Add(Me.Chart1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form_chart"
        Me.Text = "Form_chart"
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.userimage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents userimage As System.Windows.Forms.PictureBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents txtmode As System.Windows.Forms.TextBox
    Friend WithEvents txtaname As System.Windows.Forms.TextBox
    Friend WithEvents userPass As System.Windows.Forms.TextBox
    Friend WithEvents txtaccnt As System.Windows.Forms.TextBox
    Friend WithEvents userID As System.Windows.Forms.TextBox
    Friend WithEvents empID As System.Windows.Forms.TextBox
    Friend WithEvents btnRegistration As System.Windows.Forms.Button
    Friend WithEvents btnRecords As System.Windows.Forms.Button
    Friend WithEvents btnLogs As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txt_income As System.Windows.Forms.TextBox
    Friend WithEvents txt_expend As System.Windows.Forms.TextBox
    Friend WithEvents DTP As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Chart1 As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
End Class
