﻿Imports System.Data.Odbc
Public Class Form_logs


    Inherits System.Windows.Forms.Form
    Dim bytImage() As Byte

    Public Sub addDGV()
        With DataGridView1
            .Columns(0).Visible = False
            .Columns(1).Width = 175
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).Width = 178
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).Width = 252
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With
    End Sub
    Private Sub dgvrefresh()

        Using conn As New OdbcConnection("Dsn=DSNKuriak;uid=root;server=localhost;database=kuriak;port=3306")
            conn.Open()
            Dim command As New OdbcCommand("SELECT * from logs", conn)
            Dim adapter As New OdbcDataAdapter
            Dim dt As New DataTable
            adapter.SelectCommand = command
            adapter.Fill(dt)
            DataGridView1.DataSource = dt
            addDGV()
            adapter.Dispose()
            command.Dispose()
            conn.Close()
        End Using
    End Sub

    Private Sub ShowAdminPhoto()
        Try

            Dim cmd As Odbc.OdbcCommand
            Dim dr As Odbc.OdbcDataReader
            Dim eID As String

            Using cn As New OdbcConnection("Dsn=DSNKuriak;uid=root;server=localhost;database=kuriak;port=3306")
                cn.Open()

                eID = userID.Text
                cmd = cn.CreateCommand()
                cmd.CommandText = "SELECT image FROM users WHERE username = '" & userID.Text & "' "

                dr = cmd.ExecuteReader

                If dr.Read Then

                    Try
                        bytImage = CType(dr(0), Byte())
                        Dim ms As New System.IO.MemoryStream(bytImage)
                        Dim bmImage As New Bitmap(ms)
                        ms.Close()

                        userimage.Image = bmImage
                        userimage.Refresh()
                    Catch ex As Exception
                        MsgBox(ex.ToString)
                    End Try
                End If

                dr.Close()
                cn.Close()

                cmd.Dispose()
                cn.Dispose()
            End Using
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub Form_logs_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.CenterToScreen()
        dgvrefresh()
        DataGridView1.Sort(DataGridView1.Columns(0), _
System.ComponentModel.ListSortDirection.Descending)
        ShowAdminPhoto()
    End Sub

    Private Sub aname_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Using con2 As New OdbcConnection("Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306")
                con2.Open()
                Dim dt As New DateTime
                dt = Now
                Dim command3 As New OdbcCommand("INSERT INTO `logs` (`logdate`, `username`, `action`) VALUES ('" & dt.ToString & "', '" & userID.Text & "','" & userID.Text + " was logged out." & "')", con2)

                With command3.Parameters
                    .AddWithValue("@logdate", dt.ToString)
                    .AddWithValue("@user", userID.Text)
                    .AddWithValue("@action", txtmode.Text & " " & txtaname.Text & " was logged out.")
                End With
                command3.ExecuteNonQuery()
                command3.Dispose()
                con2.Close()
            End Using

        Catch ex As Exception
            MessageBox.Show(ex.Message, "ERROR12", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        Me.Close()
        Form_reg.Close()
        Form_record.Close()
        Form_chart.Close()
        FormLogin.Show()
    End Sub

    Private Sub btnRegistration_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistration.Click

        With Form_reg
            .userID.Text = userID.Text
            .Show()
        End With
        Me.Close()
    End Sub

    Private Sub btnRecords_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRecords.Click
        With Form_record
            .userID.Text = userID.Text
            .txtaccnt.Text = txtaccnt.Text
            .userPass.Text = userPass.Text
            .txtaname.Text = txtaname.Text
            .txtmode.Text = txtmode.Text
            .Show()
        End With
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        With Form_chart
            .userID.Text = userID.Text
            .txtaccnt.Text = txtaccnt.Text
            .userPass.Text = userPass.Text
            .txtaname.Text = txtaname.Text
            .txtmode.Text = txtmode.Text
            .Show()
        End With
        Me.Close()
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        With FormMenu
            .userID.Text = userID.Text
            .Show()
            Me.Close()
        End With
    End Sub
End Class