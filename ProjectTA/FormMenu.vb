﻿Imports System.Data.Odbc
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text
Public Class FormMenu
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    Sub koneksi()
        lokasiDB = "Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub

    Private Sub FormMenu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.MaximizeBox = False

        Call koneksi()
        cmd = New OdbcCommand("select level from users where username= '" & userID.Text & "'", conn)
        rd = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows() Then
            Label9.Text = rd.Item("level")
        End If
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click

    End Sub

    Private Sub PictureBox2_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles PictureBox2.MouseHover
        PictureBox1.Visible = True
        PictureBox2.Visible = False
        Label1.Visible = True
    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        Me.Hide()
        FormOrder1.Show()
    End Sub

    Private Sub PictureBox1_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles PictureBox1.MouseLeave
        PictureBox2.Visible = True
        PictureBox1.Visible = False
        Label1.Visible = False
    End Sub

    Private Sub PictureBox4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox4.Click

    End Sub

    Private Sub PictureBox4_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles PictureBox4.MouseHover
        PictureBox3.Visible = True
        PictureBox4.Visible = False
        Label2.Visible = True
    End Sub

    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click
        If Label9.Text = "1" Then
            With FormKuli
                .UserID.Text = userID.Text
                .Show()
                Me.Close()
            End With
        Else
            MsgBox("Anda Bukan Admin!", MsgBoxStyle.Exclamation, "WARNING!!!")
        End If

    End Sub

    Private Sub PictureBox3_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles PictureBox3.MouseLeave
        PictureBox4.Visible = True
        PictureBox3.Visible = False
        Label2.Visible = False
    End Sub

    Private Sub PictureBox6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox6.Click

    End Sub

    Private Sub PictureBox5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox5.Click
        FormExcel.Show()
        Me.Hide()
    End Sub

    Private Sub PictureBox6_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles PictureBox6.MouseHover
        PictureBox5.Visible = True
        PictureBox6.Visible = False
        Label5.Visible = True
    End Sub

    Private Sub PictureBox5_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles PictureBox5.MouseLeave
        PictureBox6.Visible = True
        PictureBox5.Visible = False
        Label5.Visible = False
    End Sub

    Private Sub PictureBox8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox8.Click

    End Sub

    Private Sub PictureBox8_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles PictureBox8.MouseHover
        PictureBox7.Visible = True
        PictureBox8.Visible = False
        Label3.Visible = True
    End Sub

    Private Sub PictureBox7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox7.Click
        If Label9.Text = "1" Then
            With FormHarga
                .UserID.Text = userID.Text
                .Show()
                Me.Close()
            End With
        Else
            MsgBox("Anda Bukan Admin!", MsgBoxStyle.Exclamation, "WARNING!!!")
        End If
    End Sub

    Private Sub PictureBox7_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles PictureBox7.MouseLeave
        PictureBox8.Visible = True
        PictureBox7.Visible = False
        Label3.Visible = False
    End Sub

    Private Sub PictureBox10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox10.Click

    End Sub

    Private Sub PictureBox10_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles PictureBox10.MouseHover
        PictureBox9.Visible = True
        PictureBox10.Visible = False
        Label6.Visible = True
    End Sub

    Private Sub PictureBox9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox9.Click
        If Label9.Text = "Admin" Then
            With Form_reg
                .userID.Text = userID.Text
                .Show()
                Me.Close()
            End With
        Else
            MsgBox("Anda Bukan Admin!", MsgBoxStyle.Exclamation, "WARNING!!!")
        End If
    End Sub

    Private Sub PictureBox9_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles PictureBox9.MouseLeave
        PictureBox10.Visible = True
        PictureBox9.Visible = False
        Label6.Visible = False
    End Sub

    Private Sub PictureBox12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox12.Click

    End Sub

    Private Sub PictureBox12_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles PictureBox12.MouseHover
        PictureBox11.Visible = True
        PictureBox12.Visible = False
        Label4.Visible = True
    End Sub

    Private Sub PictureBox11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox11.Click
        FormOrderan.Show()
        Me.Hide()
    End Sub

    Private Sub PictureBox11_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles PictureBox11.MouseLeave
        PictureBox12.Visible = True
        PictureBox11.Visible = False
        Label4.Visible = False
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
       
    End Sub

    Private Sub PictureBox14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox14.Click
        Diagnostics.Process.Start("http://localhost/KuriakOnline")
    End Sub

    Private Sub PictureBox13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox13.Click
        FormAbout.Show()
    End Sub

    Private Sub aname_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles aname.Click
        Try
            Using con2 As New OdbcConnection("Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306")
                con2.Open()
                Dim dt As New DateTime
                dt = Now
                Dim command3 As New OdbcCommand("INSERT INTO `logs` (`logdate`, `username`, `action`) VALUES ('" & dt.ToString & "', '" & userID.Text & "','" & userID.Text + " was logged out." & "')", con2)

                With command3.Parameters
                    .AddWithValue("@logdate", dt.ToString)
                    .AddWithValue("@user", userID.Text)
                    .AddWithValue("@action", txtmode.Text & " " & txtaname.Text & " was logged out.")
                End With
                command3.ExecuteNonQuery()
                command3.Dispose()
                con2.Close()
            End Using

        Catch ex As Exception
            MessageBox.Show(ex.Message, "ERROR12", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        '//// End Logging
        FormLogin.Show()
        Me.Close()

    End Sub

    Private Sub userID_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles userID.Click

    End Sub

    Private Sub PictureBox15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox15.Click
        FormPesan.Show()
        Me.Hide()
    End Sub
End Class