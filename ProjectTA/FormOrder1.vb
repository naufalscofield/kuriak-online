﻿Imports System.Data.Odbc
Public Class FormOrder1
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    Sub koneksi()
        lokasiDB = "Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub
    Sub aww()
        Dim dtp1 As Date
        dtp1 = DTPStart.Value
        Dim dtp2 As Date
        dtp2 = DTPend.Value
        Dim selisih As Integer
        selisih = dtp1.Day - dtp2.Day
        selisih = dtp1.Month - dtp2.Month
        selisih = dtp1.Year - dtp2.Year
    End Sub
    Sub cekkosong()
        If TextBox1.Text = "" Then TextBox1.BackColor = Color.IndianRed
        If ComboBox1.Text = "" Then ComboBox1.BackColor = Color.IndianRed
    End Sub
    Private Sub FormOrder1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ComboBox1.Text = ""
        TextBox1.Text = ""
        DTPStart.Value = Now
        DTPend.Value = Now
        Call koneksi()
        Dim cmd As New OdbcCommand("SELECT COUNT(*) FROM kuli where status = 'Y' ", conn)
        Dim i As Integer = cmd.ExecuteScalar()
        cmd = Nothing
        For value As Integer = 1 To i
            ComboBox1.Items.Add(value)
        Next
        TextBox2.Text = DTPStart.Value.Date
        TextBox3.Text = DTPend.Value.Date




    End Sub
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim count = 0
        Dim totaldays = (DTPend.Value - DTPStart.Value).Days
        Dim startDate As Date
        For i = 0 To totaldays
            Dim weekday As DayOfWeek = startDate.AddDays(i).DayOfWeek
            count += 1
        Next
        TextBox1.Text = count
        TextBox1.BackColor = Color.White
        
        
    End Sub

    Private Sub Button4_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button4.MouseHover
        Button4.BackColor = Color.Silver
        Button4.FlatAppearance.BorderColor = Color.Silver
    End Sub

    Private Sub Button4_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button4.MouseLeave
        Button4.BackColor = Color.Transparent
        Button4.FlatAppearance.BorderColor = Color.Black
    End Sub

    Private Sub ComboBox1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.Click
        ComboBox1.BackColor = Color.White
    End Sub

    Private Sub ComboBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.GotFocus
        ComboBox1.BackColor = Color.White
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        TextBox4.Text = ComboBox1.SelectedItem
    End Sub

    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click

    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        With FormMenu
            .Show()
            Me.Close()
        End With
    End Sub

    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click
        Call cekkosong()
        If ComboBox1.Text = "" Or TextBox1.Text = "" Then
            MsgBox("Harap isi data yang lengkap")
        Else
            FormOrder2.TextBox4.Text = ComboBox1.SelectedItem
            FormOrder2.TextBox5.Text = TextBox1.Text
            FormOrder2.TextBox6.Text = Format(DTPStart.Value, "yyyy/MM/dd")
            FormOrder2.TextBox7.Text = Format(DTPend.Value, "yyyy/MM/dd")
            FormOrder2.Show()
            Me.Close()
        End If
    End Sub

    Private Sub TextBox1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.Click
        TextBox1.BackColor = Color.White
    End Sub

    Private Sub TextBox1_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.Enter
        TextBox1.BackColor = Color.White
    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus
        TextBox1.BackColor = Color.White
    End Sub
    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub
End Class