﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_record
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_record))
        Me.lblPreview = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtSearch = New System.Windows.Forms.TextBox()
        Me.DataGridView = New System.Windows.Forms.DataGridView()
        Me.myimage2 = New System.Windows.Forms.PictureBox()
        Me.btnProfile = New System.Windows.Forms.Button()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.userimage = New System.Windows.Forms.PictureBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txtmode = New System.Windows.Forms.TextBox()
        Me.txtaname = New System.Windows.Forms.TextBox()
        Me.userPass = New System.Windows.Forms.TextBox()
        Me.txtaccnt = New System.Windows.Forms.TextBox()
        Me.userID = New System.Windows.Forms.TextBox()
        Me.empID = New System.Windows.Forms.TextBox()
        Me.btnRegistration = New System.Windows.Forms.Button()
        Me.btnRecords = New System.Windows.Forms.Button()
        Me.btnLogs = New System.Windows.Forms.Button()
        Me.username = New System.Windows.Forms.TextBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        CType(Me.DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.myimage2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.userimage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblPreview
        '
        Me.lblPreview.BackColor = System.Drawing.Color.DimGray
        Me.lblPreview.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPreview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lblPreview.Location = New System.Drawing.Point(10, 332)
        Me.lblPreview.Name = "lblPreview"
        Me.lblPreview.Size = New System.Drawing.Size(73, 13)
        Me.lblPreview.TabIndex = 89
        Me.lblPreview.Text = "Preview"
        Me.lblPreview.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label18
        '
        Me.Label18.BackColor = System.Drawing.Color.Black
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(382, 111)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(130, 20)
        Me.Label18.TabIndex = 97
        Me.Label18.Text = "E-Mail"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label21
        '
        Me.Label21.BackColor = System.Drawing.Color.Black
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(513, 111)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(107, 20)
        Me.Label21.TabIndex = 96
        Me.Label21.Text = "Phone Number"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.Color.Black
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(224, 111)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(157, 20)
        Me.Label17.TabIndex = 93
        Me.Label17.Text = "Full Name"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.Color.Black
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(98, 111)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(125, 20)
        Me.Label16.TabIndex = 92
        Me.Label16.Text = "Username"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtSearch
        '
        Me.txtSearch.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSearch.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSearch.ForeColor = System.Drawing.Color.DimGray
        Me.txtSearch.Location = New System.Drawing.Point(97, 325)
        Me.txtSearch.Margin = New System.Windows.Forms.Padding(8, 3, 3, 3)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(524, 20)
        Me.txtSearch.TabIndex = 99
        Me.txtSearch.Text = "  Enter Keyword To Search"
        Me.txtSearch.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DataGridView
        '
        Me.DataGridView.AllowUserToAddRows = False
        Me.DataGridView.AllowUserToDeleteRows = False
        Me.DataGridView.AllowUserToResizeColumns = False
        Me.DataGridView.AllowUserToResizeRows = False
        Me.DataGridView.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.DataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView.ColumnHeadersVisible = False
        Me.DataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.DataGridView.GridColor = System.Drawing.Color.Gray
        Me.DataGridView.Location = New System.Drawing.Point(98, 131)
        Me.DataGridView.MultiSelect = False
        Me.DataGridView.Name = "DataGridView"
        Me.DataGridView.ReadOnly = True
        Me.DataGridView.RowHeadersVisible = False
        Me.DataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.DataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView.Size = New System.Drawing.Size(521, 192)
        Me.DataGridView.TabIndex = 90
        '
        'myimage2
        '
        Me.myimage2.BackColor = System.Drawing.Color.MediumSlateBlue
        Me.myimage2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.myimage2.Image = Global.ProjectTA.My.Resources.Resources.noimg
        Me.myimage2.Location = New System.Drawing.Point(10, 263)
        Me.myimage2.Name = "myimage2"
        Me.myimage2.Size = New System.Drawing.Size(73, 69)
        Me.myimage2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.myimage2.TabIndex = 88
        Me.myimage2.TabStop = False
        '
        'btnProfile
        '
        Me.btnProfile.BackColor = System.Drawing.Color.DimGray
        Me.btnProfile.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnProfile.Image = CType(resources.GetObject("btnProfile.Image"), System.Drawing.Image)
        Me.btnProfile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnProfile.Location = New System.Drawing.Point(10, 178)
        Me.btnProfile.Name = "btnProfile"
        Me.btnProfile.Size = New System.Drawing.Size(75, 23)
        Me.btnProfile.TabIndex = 86
        Me.btnProfile.Text = "Profile"
        Me.btnProfile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnProfile.UseCompatibleTextRendering = True
        Me.btnProfile.UseVisualStyleBackColor = False
        '
        'btnRefresh
        '
        Me.btnRefresh.BackColor = System.Drawing.Color.DimGray
        Me.btnRefresh.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnRefresh.Image = CType(resources.GetObject("btnRefresh.Image"), System.Drawing.Image)
        Me.btnRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRefresh.Location = New System.Drawing.Point(10, 109)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(75, 23)
        Me.btnRefresh.TabIndex = 81
        Me.btnRefresh.Text = "Refresh"
        Me.btnRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnRefresh.UseCompatibleTextRendering = True
        Me.btnRefresh.UseVisualStyleBackColor = False
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.DimGray
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnEdit.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnEdit.Image = CType(resources.GetObject("btnEdit.Image"), System.Drawing.Image)
        Me.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEdit.Location = New System.Drawing.Point(10, 132)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 23)
        Me.btnEdit.TabIndex = 82
        Me.btnEdit.Text = "Modify"
        Me.btnEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnEdit.UseCompatibleTextRendering = True
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.DimGray
        Me.btnDelete.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnDelete.Image = CType(resources.GetObject("btnDelete.Image"), System.Drawing.Image)
        Me.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDelete.Location = New System.Drawing.Point(10, 155)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 23)
        Me.btnDelete.TabIndex = 83
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnDelete.UseCompatibleTextRendering = True
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.PictureBox1)
        Me.Panel2.Controls.Add(Me.userimage)
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(645, 73)
        Me.Panel2.TabIndex = 105
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.ProjectTA.My.Resources.Resources.kuriak_logo
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(117, 79)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'userimage
        '
        Me.userimage.BackColor = System.Drawing.Color.MediumSlateBlue
        Me.userimage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.userimage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.userimage.Image = Global.ProjectTA.My.Resources.Resources.noimg
        Me.userimage.Location = New System.Drawing.Point(584, 9)
        Me.userimage.Name = "userimage"
        Me.userimage.Size = New System.Drawing.Size(48, 42)
        Me.userimage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.userimage.TabIndex = 35
        Me.userimage.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Silver
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.txtmode)
        Me.Panel1.Controls.Add(Me.txtaname)
        Me.Panel1.Controls.Add(Me.userPass)
        Me.Panel1.Controls.Add(Me.txtaccnt)
        Me.Panel1.Controls.Add(Me.userID)
        Me.Panel1.Controls.Add(Me.empID)
        Me.Panel1.Controls.Add(Me.btnRegistration)
        Me.Panel1.Controls.Add(Me.btnRecords)
        Me.Panel1.Controls.Add(Me.btnLogs)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 390)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(637, 64)
        Me.Panel1.TabIndex = 141
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.MediumOrchid
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.Teal
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Image = Global.ProjectTA.My.Resources.Resources.chart_32
        Me.Button1.Location = New System.Drawing.Point(380, 5)
        Me.Button1.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Button1.Size = New System.Drawing.Size(65, 56)
        Me.Button1.TabIndex = 148
        Me.Button1.Text = "CHART"
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Button1.UseVisualStyleBackColor = False
        '
        'txtmode
        '
        Me.txtmode.BackColor = System.Drawing.Color.Gray
        Me.txtmode.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtmode.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtmode.Location = New System.Drawing.Point(622, 5)
        Me.txtmode.Name = "txtmode"
        Me.txtmode.Size = New System.Drawing.Size(10, 20)
        Me.txtmode.TabIndex = 60
        Me.txtmode.Visible = False
        '
        'txtaname
        '
        Me.txtaname.BackColor = System.Drawing.Color.Gray
        Me.txtaname.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtaname.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtaname.Location = New System.Drawing.Point(611, 5)
        Me.txtaname.Name = "txtaname"
        Me.txtaname.Size = New System.Drawing.Size(10, 20)
        Me.txtaname.TabIndex = 59
        Me.txtaname.Visible = False
        '
        'userPass
        '
        Me.userPass.BackColor = System.Drawing.Color.Gray
        Me.userPass.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.userPass.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.userPass.Location = New System.Drawing.Point(600, 5)
        Me.userPass.Name = "userPass"
        Me.userPass.Size = New System.Drawing.Size(10, 20)
        Me.userPass.TabIndex = 39
        Me.userPass.Visible = False
        '
        'txtaccnt
        '
        Me.txtaccnt.BackColor = System.Drawing.Color.Gray
        Me.txtaccnt.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtaccnt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtaccnt.Location = New System.Drawing.Point(590, 5)
        Me.txtaccnt.Name = "txtaccnt"
        Me.txtaccnt.Size = New System.Drawing.Size(10, 20)
        Me.txtaccnt.TabIndex = 38
        Me.txtaccnt.Visible = False
        '
        'userID
        '
        Me.userID.BackColor = System.Drawing.Color.Gray
        Me.userID.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.userID.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.userID.Location = New System.Drawing.Point(580, 5)
        Me.userID.Name = "userID"
        Me.userID.Size = New System.Drawing.Size(10, 20)
        Me.userID.TabIndex = 37
        Me.userID.Visible = False
        '
        'empID
        '
        Me.empID.BackColor = System.Drawing.Color.Gray
        Me.empID.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.empID.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.empID.Location = New System.Drawing.Point(570, 5)
        Me.empID.Name = "empID"
        Me.empID.Size = New System.Drawing.Size(10, 20)
        Me.empID.TabIndex = 36
        Me.empID.Visible = False
        '
        'btnRegistration
        '
        Me.btnRegistration.BackColor = System.Drawing.Color.Gold
        Me.btnRegistration.FlatAppearance.BorderColor = System.Drawing.Color.Teal
        Me.btnRegistration.FlatAppearance.BorderSize = 0
        Me.btnRegistration.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnRegistration.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.btnRegistration.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRegistration.ForeColor = System.Drawing.Color.Black
        Me.btnRegistration.Image = CType(resources.GetObject("btnRegistration.Image"), System.Drawing.Image)
        Me.btnRegistration.Location = New System.Drawing.Point(182, 5)
        Me.btnRegistration.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnRegistration.Name = "btnRegistration"
        Me.btnRegistration.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnRegistration.Size = New System.Drawing.Size(65, 56)
        Me.btnRegistration.TabIndex = 19
        Me.btnRegistration.Text = "REGISTER"
        Me.btnRegistration.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnRegistration.UseVisualStyleBackColor = False
        '
        'btnRecords
        '
        Me.btnRecords.BackColor = System.Drawing.Color.Tomato
        Me.btnRecords.FlatAppearance.BorderColor = System.Drawing.Color.Teal
        Me.btnRecords.FlatAppearance.BorderSize = 0
        Me.btnRecords.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnRecords.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.btnRecords.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRecords.ForeColor = System.Drawing.Color.Black
        Me.btnRecords.Image = CType(resources.GetObject("btnRecords.Image"), System.Drawing.Image)
        Me.btnRecords.Location = New System.Drawing.Point(248, 5)
        Me.btnRecords.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnRecords.Name = "btnRecords"
        Me.btnRecords.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnRecords.Size = New System.Drawing.Size(65, 56)
        Me.btnRecords.TabIndex = 20
        Me.btnRecords.Text = "RECORDS"
        Me.btnRecords.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnRecords.UseVisualStyleBackColor = False
        '
        'btnLogs
        '
        Me.btnLogs.BackColor = System.Drawing.Color.SteelBlue
        Me.btnLogs.FlatAppearance.BorderColor = System.Drawing.Color.Teal
        Me.btnLogs.FlatAppearance.BorderSize = 0
        Me.btnLogs.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnLogs.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.btnLogs.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogs.ForeColor = System.Drawing.Color.Black
        Me.btnLogs.Image = CType(resources.GetObject("btnLogs.Image"), System.Drawing.Image)
        Me.btnLogs.Location = New System.Drawing.Point(314, 6)
        Me.btnLogs.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnLogs.Name = "btnLogs"
        Me.btnLogs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnLogs.Size = New System.Drawing.Size(65, 56)
        Me.btnLogs.TabIndex = 23
        Me.btnLogs.Text = "LOGS"
        Me.btnLogs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnLogs.UseVisualStyleBackColor = False
        '
        'username
        '
        Me.username.Location = New System.Drawing.Point(107, 88)
        Me.username.Name = "username"
        Me.username.Size = New System.Drawing.Size(10, 20)
        Me.username.TabIndex = 142
        Me.username.Visible = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(10, 14)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(42, 38)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 56
        Me.PictureBox2.TabStop = False
        '
        'Form_record
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ProjectTA.My.Resources.Resources.KURIAK_BG
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(637, 454)
        Me.Controls.Add(Me.username)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.myimage2)
        Me.Controls.Add(Me.lblPreview)
        Me.Controls.Add(Me.btnProfile)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.txtSearch)
        Me.Controls.Add(Me.DataGridView)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form_record"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form_record"
        CType(Me.DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.myimage2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.userimage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents myimage2 As System.Windows.Forms.PictureBox
    Friend WithEvents lblPreview As System.Windows.Forms.Label
    Friend WithEvents btnProfile As System.Windows.Forms.Button
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents DataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents userimage As System.Windows.Forms.PictureBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtmode As System.Windows.Forms.TextBox
    Friend WithEvents txtaname As System.Windows.Forms.TextBox
    Friend WithEvents userPass As System.Windows.Forms.TextBox
    Friend WithEvents txtaccnt As System.Windows.Forms.TextBox
    Friend WithEvents userID As System.Windows.Forms.TextBox
    Friend WithEvents empID As System.Windows.Forms.TextBox
    Friend WithEvents btnRegistration As System.Windows.Forms.Button
    Friend WithEvents btnRecords As System.Windows.Forms.Button
    Friend WithEvents btnLogs As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents username As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
End Class
