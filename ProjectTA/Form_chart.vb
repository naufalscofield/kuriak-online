﻿Imports System.Data.Odbc
Imports System.Data
Imports System.Windows.Forms.DataVisualization.Charting

Public Class Form_chart

    Inherits System.Windows.Forms.Form
    Dim bytImage() As Byte

    Dim conn As OdbcConnection
    Dim cmd As OdbcCommand
    Dim da As OdbcDataAdapter
    Dim table As DataTable
    Dim lokasiDB As String
    Dim ds As New DataSet

    Dim rd As OdbcDataReader


    Dim dt As Date = Date.Today

    Sub koneksi()
        lokasiDB = "Dsn=DSNKuriak;uid=root;server=localhost;database=kuriak;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then conn.Open()
    End Sub
    Sub refresdata()
        koneksi()
        da = New OdbcDataAdapter("select * from chart", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "chart")
    End Sub

    Private Sub ShowAdminPhoto()
        Try

            Dim cmd As Odbc.OdbcCommand
            Dim dr As Odbc.OdbcDataReader
            Dim eID As String

            Using cn As New OdbcConnection("Dsn=DSNKuriak;uid=root;server=localhost;database=kuriak;port=3306")
                cn.Open()

                eID = userID.Text
                cmd = cn.CreateCommand()
                cmd.CommandText = "SELECT image FROM users WHERE username = '" & userID.Text & "' "

                dr = cmd.ExecuteReader

                If dr.Read Then

                    Try
                        bytImage = CType(dr(0), Byte())
                        Dim ms As New System.IO.MemoryStream(bytImage)
                        Dim bmImage As New Bitmap(ms)
                        ms.Close()

                        userimage.Image = bmImage
                        userimage.Refresh()
                    Catch ex As Exception
                        MsgBox(ex.ToString)
                    End Try
                End If

                dr.Close()
                cn.Close()

                cmd.Dispose()
                cn.Dispose()
            End Using
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub Form_chart_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.CenterToScreen()
        ShowAdminPhoto()
        refresdata()

        'MENAMPILKAN DATA  PADA CHART
        Dim strConn As String = "Dsn=DSNKuriak;uid=root;server=localhost;database=kuriak;port=3306"

        Dim conn As New OdbcConnection(strConn)
        Dim sqlProducts As String = "SELECT * FROM chart"
        Dim da As New OdbcDataAdapter(sqlProducts, conn)
        Dim ds As New DataSet()
        da.Fill(ds, "chart")

        Chart1.Series("Pengeluaran").XValueMember = "bulantahun"
        Chart1.Series("Pengeluaran").YValueMembers = "pengeluaran"

        Chart1.Series("Pemasukan").XValueMember = "bulantahun"
        Chart1.Series("Pemasukan").YValueMembers = "pemasukan"

        Chart1.DataSource = ds.Tables("chart")
        '----------------------------------------------------'

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If txt_income.Text = "" Or txt_expend.Text = "" Then
            MsgBox("Data is not complete!", MsgBoxStyle.Critical, "Warning!!!")

        Else
            Dim Pesan As String = MsgBox("Are you sure the following data, Input Date: " & DTP.Value & ", Income : " & txt_income.Text & " , Expenditure : " & txt_expend.Text & "?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirmation")
            If Pesan = vbYes Then
                Call koneksi()
                Dim simpan As String = "insert into chart values('','" & DTP.Value & "','" & txt_income.Text & "','" & txt_expend.Text & "')"
                cmd = New OdbcCommand(simpan, conn)
                cmd.ExecuteNonQuery()

                MsgBox("Data successfully saved!", MsgBoxStyle.Information, "Infomation")
                Call refresdata()

            Else
            End If
        End If
        Try
            Using con2 As New OdbcConnection("Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306")
                con2.Open()
                Dim dt As New DateTime
                dt = Now
                Dim command3 As New OdbcCommand("INSERT INTO `logs` (`logdate`, `username`, `action`) VALUES ('" & dt.ToString & "', '" & userID.Text & "','" & userID.Text + " was Updated Financial." & "')", con2)

                With command3.Parameters
                    .AddWithValue("@logdate", dt.ToString)
                    .AddWithValue("@user", userID.Text)
                    .AddWithValue("@action", txtmode.Text & " " & txtaname.Text & " was logged out.")
                End With
                command3.ExecuteNonQuery()
                command3.Dispose()
                con2.Close()
            End Using
        Catch ex As System.Data.Odbc.OdbcException
            MessageBox.Show(ex.Message, "ERROR12", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        koneksi()
        da = New OdbcDataAdapter("select * from chart", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "chart")

    End Sub

    Private Sub aname_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Using con2 As New OdbcConnection("Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306")
                con2.Open()
                Dim dt As New DateTime
                dt = Now
                Dim command3 As New OdbcCommand("INSERT INTO `logs` (`logdate`, `username`, `action`) VALUES ('" & dt.ToString & "', '" & userID.Text & "','" & userID.Text + " was logged out." & "')", con2)

                With command3.Parameters
                    .AddWithValue("@logdate", dt.ToString)
                    .AddWithValue("@user", userID.Text)
                    .AddWithValue("@action", txtmode.Text & " " & txtaname.Text & " was logged out.")
                End With
                command3.ExecuteNonQuery()
                command3.Dispose()
                con2.Close()
            End Using

        Catch ex As Exception
            MessageBox.Show(ex.Message, "ERROR12", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        '//// End Logging
        Me.Close()
        Form_record.Close()
        Form_logs.Close()
        Form_reg.Close()
        FormLogin.Show()

    End Sub

    Private Sub txt_income_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_income.KeyPress
        Dim strValid As String = "0123456789-.,"

        Dim x As Long = InStr(strValid, e.KeyChar)

        If x = 0 And Asc(e.KeyChar) <> 8 And Asc(e.KeyChar) <> 32 Then

            e.KeyChar = ""

        End If
    End Sub

    Private Sub txt_expend_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_expend.KeyPress
        Dim strValid As String = "0123456789-.,"

        Dim x As Long = InStr(strValid, e.KeyChar)

        If x = 0 And Asc(e.KeyChar) <> 8 And Asc(e.KeyChar) <> 32 Then

            e.KeyChar = ""

        End If
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub btnLogs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogs.Click
        With Form_logs
            .userID.Text = userID.Text
            .txtaccnt.Text = txtaccnt.Text
            .userPass.Text = userPass.Text
            .txtaname.Text = txtaname.Text
            .txtmode.Text = txtmode.Text
            .Show()
        End With
        Me.Close()
    End Sub

    Private Sub btnRecords_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRecords.Click
        With Form_record
            .userID.Text = userID.Text
            .txtaccnt.Text = txtaccnt.Text
            .userPass.Text = userPass.Text
            .txtaname.Text = txtaname.Text
            .txtmode.Text = txtmode.Text
            .Show()
        End With
        Me.Close()
    End Sub

    Private Sub btnRegistration_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistration.Click
        With Form_reg
            .userID.Text = userID.Text
            .txtaccnt.Text = txtaccnt.Text
            .userPass.Text = userPass.Text
            .txtaname.Text = txtaname.Text
            .txtmode.Text = txtmode.Text
            .Show()
        End With
        Me.Close()
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        With FormMenu
            .userID.Text = userID.Text
            .Show()
            Me.Close()
        End With
    End Sub
End Class