﻿Imports System.Data.Odbc
Imports System.Runtime.InteropServices
Imports System.IO

Public Class Form_reg
    Inherits System.Windows.Forms.Form
    Dim bytImage() As Byte

    Dim conn As OdbcConnection
    Dim cmd As OdbcCommand
    Dim rdr As OdbcDataReader

    Private Sub Form_reg_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ShowAdminPhoto()
       
        Dim DriverName As String = Space(80)
        Dim DriverVersion As String = Space(80)
        For i As Integer = 0 To 9
            If capGetDriverDescriptionA(i, DriverName, 80, DriverVersion, 80) Then
                ListBox1.Items.Add(DriverName.Trim)
            End If
        Next


    End Sub

    'CAMERA

    Const WM_CAP_START = &H400S
    Const WS_CHILD = &H40000000
    Const WS_VISIBLE = &H10000000


    Const WM_CAP_DRIVER_CONNECT = WM_CAP_START + 10
    Const WM_CAP_DRIVER_DISCONNECT = WM_CAP_START + 11
    Const WM_CAP_EDIT_COPY = WM_CAP_START + 30
    Const WM_CAP_SEQUENCE = WM_CAP_START + 62
    Const WM_CAP_FILE_SAVEAS = WM_CAP_START + 23


    Const WM_CAP_SET_SCALE = WM_CAP_START + 53
    Const WM_CAP_SET_PREVIEWRATE = WM_CAP_START + 52
    Const WM_CAP_SET_PREVIEW = WM_CAP_START + 50


    Const SWP_NOMOVE = &H2S
    Const SWP_NOSIZE = 1
    Const SWP_NOZORDER = &H4S
    Const HWND_BOTTOM = 1


    Declare Function capGetDriverDescriptionA Lib "avicap32.dll" _
       (ByVal wDriverIndex As Short, _
        ByVal lpszName As String, ByVal cbName As Integer, ByVal lpszVer As String, _
        ByVal cbVer As Integer) As Boolean


    Declare Function capCreateCaptureWindowA Lib "avicap32.dll" _
       (ByVal lpszWindowName As String, ByVal dwStyle As Integer, _
        ByVal x As Integer, ByVal y As Integer, ByVal nWidth As Integer, _
        ByVal nHeight As Short, ByVal hWnd As Integer, _
        ByVal nID As Integer) As Integer


    Declare Function SendMessage Lib "user32" Alias "SendMessageA" _
       (ByVal hwnd As Integer, ByVal Msg As Integer, ByVal wParam As Integer, _
       <MarshalAs(UnmanagedType.AsAny)> ByVal lParam As Object) As Integer


    Declare Function SetWindowPos Lib "user32" Alias "SetWindowPos" _
       (ByVal hwnd As Integer, _
        ByVal hWndInsertAfter As Integer, ByVal x As Integer, ByVal y As Integer, _
        ByVal cx As Integer, ByVal cy As Integer, ByVal wFlags As Integer) As Integer


    Declare Function DestroyWindow Lib "user32" (ByVal hndw As Integer) As Boolean


    Dim VideoSource As Integer
    Dim hWnd As Integer


    'CAMERA END

    Private Sub ShowAdminPhoto()
        Try

            Dim cmd As odbc.odbcCommand
            Dim dr As odbc.odbcDataReader
            Dim eID As String

            Using cn As New OdbcConnection("Dsn=DSNKuriak;uid=root;server=localhost;database=kuriak;port=3306")
                cn.Open()

                eID = userID.Text
                cmd = cn.CreateCommand()
                cmd.CommandText = "SELECT image FROM users WHERE username = '" & userID.Text & "' "

                dr = cmd.ExecuteReader

                If dr.Read Then

                    Try
                        bytImage = CType(dr(0), Byte())
                        Dim ms As New System.IO.MemoryStream(bytImage)
                        Dim bmImage As New Bitmap(ms)
                        ms.Close()

                        userimage.Image = bmImage
                        userimage.Refresh()
                    Catch ex As Exception
                        MsgBox(ex.ToString)
                    End Try
                End If

                dr.Close()
                cn.Close()

                cmd.Dispose()
                cn.Dispose()
            End Using
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub btnRecords_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRecords.Click
        With Form_record
            .userID.Text = userID.Text
            .txtaccnt.Text = txtaccnt.Text
            .userPass.Text = userPass.Text
            .txtaname.Text = txtaname.Text
            .txtmode.Text = txtmode.Text
            .Show()
        End With
        Me.Close()
    End Sub

    Private Sub aname_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Using con2 As New OdbcConnection("Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306")
                con2.Open()
                Dim dt As New DateTime
                dt = Now
                Dim command3 As New OdbcCommand("INSERT INTO `logs` (`logdate`, `username`, `action`) VALUES ('" & dt.ToString & "', '" & userID.Text & "','" & userID.Text + " was logged out." & "')", con2)

                With command3.Parameters
                    .AddWithValue("@logdate", dt.ToString)
                    .AddWithValue("@user", userID.Text)
                    .AddWithValue("@action", txtmode.Text & " " & txtaname.Text & " was logged out.")
                End With
                command3.ExecuteNonQuery()
                command3.Dispose()
                con2.Close()
            End Using

        Catch ex As Exception
            MessageBox.Show(ex.Message, "ERROR12", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        '//// End Logging
        Me.Close()
        Form_record.Close()
        Form_logs.Close()
        Form_chart.Close()
        FormLogin.Show()
    End Sub

    Private Sub select_image_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles select_image.Click
        With ofdImage
            .InitialDirectory = "C:"
            .Filter = "Only Picture With JPG/JPEG/PNG Extension|*.jpg;*.jpeg;*.png"
            .FileName = Nothing
        End With
        If ofdImage.ShowDialog = Windows.Forms.DialogResult.OK Then
            myimage.Image = Image.FromFile(ofdImage.FileName)
            myimage.Tag = ofdImage.FileName
        Else
            myimage.Tag = ""
        End If
    End Sub

    Private Sub btnRegister_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegister.Click
        If btnRegister.Text = "Register" Then
              If Username.Text = "" Then
                MessageBox.Show("Insert Username Please!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            ElseIf password.Text = "" Then
                MessageBox.Show("Insert Password Please!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            ElseIf email.Text = "" Then
                MessageBox.Show("Insert Email Please!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            ElseIf nama.Text = "" Then
                MessageBox.Show("Insert Name Please!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            ElseIf pn.Text = "" Then
                MessageBox.Show("Insert Phone Number Please!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            Else
                            Try
                    Using conn As New OdbcConnection("Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306")
                        conn.Open()
                        Try
                            Dim ms As New System.IO.MemoryStream
                            Dim bmpImage As New Bitmap(myimage.Image)

                            bmpImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)
                            bytImage = ms.ToArray()
                            ms.Close()
                        Catch ex As Exception
                            MsgBox(ex.ToString)
                        End Try
                        Dim command As New OdbcCommand("insert into users (username, password, nama_lengkap, email, no_telp, image) values ('" & username.Text & "', '" & password.Text & "', '" & nama.Text & "','" & email.Text & "', '" & pn.Text + "', '" & bytImage.ToString & "')", conn)
                        With command.Parameters
                            .AddWithValue("@fname", password.Text)
                            .AddWithValue("@address", nama.Text)
                            .AddWithValue("@contact", email.Text)
                            .AddWithValue("@myimage", bytImage)

                        End With

                        '//// Logging action
                        Try
                            Using con2 As New OdbcConnection("Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306")
                                con2.Open()
                                Dim dt As New DateTime
                                dt = Now
                                Dim command3 As New OdbcCommand("INSERT INTO `logs` (`logdate`, `username`, `action`) VALUES ('" & dt.ToString & "', '" & userID.Text & "', '" & userID.Text + "registered a employee named: " + nama.Text & "')", con2)

                                With command3.Parameters
                                    .AddWithValue("@logdate", dt.ToString)
                                    .AddWithValue("@accnt", userID.Text)
                                    .AddWithValue("@aname", txtaname.Text)
                                    .AddWithValue("@action", txtmode.Text & " " & txtaname.Text & " " & "registered a regular employee named: " + userID.Text)
                                End With
                                command3.ExecuteNonQuery()
                                command3.Dispose()
                                con2.Close()
                            End Using
                        Catch ex As Exception
                            MessageBox.Show(ex.Message, "ERROR12", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End Try
                        '//// End Logging

                        command.ExecuteNonQuery()
                        MessageBox.Show("Employee's Informations Successfuly Recorded!", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        command.Dispose()
                        conn.Close()
                        password.Text = ""
                        username.Text = ""
                        email.Text = ""
                        nama.Text = ""
                        email.Text = ""
                        username.Text = ""
                        password.Text = ""
                        pn.Text = ""
                        myimage.Image = ProjectTA.My.Resources.noimg
                    End Using
                            Catch ex As Exception
                                MessageBox.Show(ex.Message, "ERROR12", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End Try
                        End If

            '// Updating Employee's data

        ElseIf btnRegister.Text = "Update" Then

            If Username.Text = "" Then
                MessageBox.Show("Insert Username Please!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            ElseIf Password.Text = "" Then
                MessageBox.Show("Insert Password Please!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            ElseIf email.Text = "" Then
                MessageBox.Show("Insert Email Please!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            ElseIf nama.Text = "" Then
                MessageBox.Show("Insert Name Please!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            ElseIf pn.Text = "" Then
                MessageBox.Show("Insert Phone Number Please!", "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            Else
                            Try
                    Using conn As New OdbcConnection("Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306")
                        conn.Open()
                        Try
                            Dim ms As New System.IO.MemoryStream
                            Dim bmpImage As New Bitmap(myimage.Image)

                            bmpImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)
                            bytImage = ms.ToArray()
                            ms.Close()
                        Catch ex As Exception
                            MsgBox(ex.ToString)
                        End Try
                        Dim command As New OdbcCommand("update users set password = '" & password.Text & "', no_telp = '" & pn.Text & "', image = '" & bytImage.ToString & "' where username = '" & username.Text & "'", conn)
                        With command.Parameters
                            .AddWithValue("@fname", password.Text)
                            .AddWithValue("@address", nama.Text)
                            .AddWithValue("@contact", email.Text)
                            
                            .AddWithValue("@myimage", bytImage)
                            .AddWithValue("@id", empID.Text)

                        End With

                        '//// Logging action
                        Try
                            Using con2 As New OdbcConnection("Dsn=DSNperusahaan;server=localhost;uid=root;database=db_perusahaan;port=3306")
                                con2.Open()
                                Dim dt As New DateTime
                                dt = Now
                                Dim command3 As New OdbcCommand("INSERT INTO `logs` (`logdate`, `username`, `action`) VALUES ('" & dt.ToString & "', '" & userID.Text & "', '" & userID.Text + "Updated a record of: " + nama.Text & "')", con2)

                                With command3.Parameters
                                    .AddWithValue("@logdate", dt.ToString)
                                    .AddWithValue("@accnt", userID.Text)
                                    .AddWithValue("@aname", txtaname.Text)
                                    .AddWithValue("@action", txtmode.Text & " " & txtaname.Text & " " & "updated a record of: " + userID.Text)
                                End With
                                command3.ExecuteNonQuery()
                                command3.Dispose()
                                con2.Close()
                            End Using
                        Catch ex As Exception
                            MessageBox.Show(ex.Message, "ERROR12", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End Try
                        '//// End Logging

                        command.ExecuteNonQuery()
                        MessageBox.Show("Employee's Informations Successfuly Updated!", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        command.Dispose()
                        conn.Close()
                        password.Text = ""
                        nama.Text = ""
                        email.Text = ""
                        pn.Text = ""
                        myimage.Image = ProjectTA.My.Resources.noimg
                        btnRegister.Text = "Register"
                        btnReset.Text = "Reset"
                        btnRegistration.Text = "Select Photo"
                    End Using
                            Catch ex As Exception
                                MessageBox.Show(ex.Message, "ERROR12", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End Try
                        End If

        End If
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        If btnReset.Text = "Cancel" Then
            nama.Text = ""
            username.Text = ""
            password.Text = ""
            email.Text = ""
            pn.Text = ""
            myimage.Image = ProjectTA.My.Resources.noimg
            btnRegister.Text = "Register"
            btnReset.Text = "Reset"
            btnRegistration.Text = "Select Photo"

        Else
            nama.Text = ""
            username.Text = ""
            password.Text = ""
            email.Text = ""
            pn.Text = ""
            myimage.Image = ProjectTA.My.Resources.noimg
            btnRegister.Text = "Register"
        End If

    End Sub

    Private Sub btnLogs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogs.Click

        With Form_logs
            .userID.Text = userID.Text
            .txtaccnt.Text = txtaccnt.Text
            .userPass.Text = userPass.Text
            .txtaname.Text = txtaname.Text
            .txtmode.Text = txtmode.Text
            .Show()
        End With
        Me.Close()
    End Sub

    Private Sub OpenPreviewWindow()
        Dim iHeight As Integer = myimage.Height
        Dim iWidth As Integer = myimage.Width
        hHwnd = capCreateCaptureWindowA(iDevice, WS_VISIBLE Or WS_CHILD, 0, 0, 640, 480, myimage.Handle.ToInt32, 0)
        If SendMessage(hHwnd, WM_Cap_Paki_CONNECT, iDevice, 0) Then
            SendMessage(hHwnd, WM_Cap_SET_SCALE, True, 0)
            SendMessage(hHwnd, WM_Cap_SET_PREVIEWRATE, 66, 0)
            SendMessage(hHwnd, WM_Cap_SET_PREVIEW, True, 0)
            SetWindowPos(hHwnd, HWND_BOTTOM, 0, 0, myimage.Width, myimage.Height, SWP_NOMOVE Or SWP_NOZORDER)
        Else
            DestroyWindow(hHwnd)
        End If
    End Sub

    Private Sub ClosePreviewWindow()
        SendMessage(hHwnd, WM_Cap_Paki_DISCONNECT, iDevice, 0)
        DestroyWindow(hHwnd)
    End Sub

    Private Sub btnCapture_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCapture.Click
        If btnCapture.Text = "Camera" Then
            Call OpenPreviewWindow()
            btnCapture.Text = "Capture"
            btnReset.Enabled = False
            btnRegister.Enabled = False
            select_image.Enabled = False

        ElseIf btnCapture.Text = "Capture" Then
            Dim Data As IDataObject
            Dim Bmap As Image
            SendMessage(hHwnd, WM_CAP_EDIT_COPY, 0, 0)
            Data = Clipboard.GetDataObject()
            If Data.GetDataPresent(GetType(System.Drawing.Bitmap)) Then
                Bmap = CType(Data.GetData(GetType(System.Drawing.Bitmap)), Image)
                myimage.Image = Bmap
                ' Bmap.Save("C:\Users\ALDI\Documents\Visual Studio 2010\Projects\Aplikasi Data Perusahaan\Data  Perusahaan\Resources\Member\" & Nip.Text & ".jpg")
                ClosePreviewWindow()
            End If
            btnCapture.Text = "Camera"
            btnReset.Enabled = True
            btnRegister.Enabled = True
            select_image.Enabled = True
            Call ClosePreviewWindow()

        End If
    End Sub

    Private Sub ListBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        SendMessage(hWnd, WM_CAP_DRIVER_DISCONNECT, VideoSource, 0)
        DestroyWindow(hWnd)


        VideoSource = ListBox1.SelectedIndex


        hWnd = capCreateCaptureWindowA(VideoSource, WS_VISIBLE Or WS_CHILD, 0, 0, 0, _
            0, myimage.Handle.ToInt32, 0)
        If SendMessage(hWnd, WM_CAP_DRIVER_CONNECT, VideoSource, 0) Then


            SendMessage(hWnd, WM_CAP_SET_SCALE, True, 0)


            SendMessage(hWnd, WM_CAP_SET_PREVIEWRATE, 30, 0)


            SendMessage(hWnd, WM_CAP_SET_PREVIEW, True, 0)


            SetWindowPos(hWnd, HWND_BOTTOM, 0, 0, _
               myimage.Width, myimage.Height, _
               SWP_NOMOVE Or SWP_NOZORDER)
        Else


            DestroyWindow(hWnd)
        End If
    End Sub


    Private Sub btnHarga_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        With Form_chart
            .userID.Text = userID.Text
            .txtaccnt.Text = txtaccnt.Text
            .userPass.Text = userPass.Text
            .txtaname.Text = txtaname.Text
            .txtmode.Text = txtmode.Text
            .Show()
        End With
        Me.Close()
    End Sub

    Private Sub pn_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles pn.KeyPress
        Dim strValid As String = "0123456789-.,"

        Dim x As Long = InStr(strValid, e.KeyChar)

        If x = 0 And Asc(e.KeyChar) <> 8 And Asc(e.KeyChar) <> 32 Then

            e.KeyChar = ""

        End If
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        With FormMenu
            .userID.Text = userID.Text
            .Show()
            Me.Close()
        End With
    End Sub
End Class