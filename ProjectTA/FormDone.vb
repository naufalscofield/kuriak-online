﻿Imports System.Data.Odbc
Public Class FormDone
    Dim conn As New OdbcConnection
    Dim cmd, count As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    Sub koneksi()
        lokasiDB = "Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub
    Public Sub addDGV()

        With dgv
            'menghilangkan row header
            .RowHeadersVisible = False
            'menghilangkan colum header
            .ColumnHeadersVisible = False
            'agar ketika baris di datagrid diklik 1x melalui mouse atau kursor, satu baris penuh yang pilih, bukan per kolom
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Columns(0).Width = 56
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 70
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).Width = 126
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).Width = 144
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).Width = 236
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(5).Width = 79
            .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(6).Width = 79
            .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter



        End With
    End Sub

    Sub refreshdata()
        Call koneksi()
        da = New OdbcDataAdapter("select `order`.id_order,kuli.id_kuli,`order`.pengorder,kuli.nama_kuli,`order`.alamat_kerja,`order`.awal_kerja,`order`.akhir_kerja from order_detail inner join `order` on order_detail.id_order=`order`.id_order inner join kuli on order_detail.id_kuli=kuli.id_kuli where `order`.akhir_kerja = '" & Label29.Text & "' and kuli.status = 'N' ", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "order_detail")
        dgv.DataSource = (ds.Tables("order_detail"))
        
    End Sub
   


    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Hide()
        FormOrderan.Show()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Hide()
        FormProccess.Show()
    End Sub

    Private Sub FormDone_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Label29.Text = Format(Now, "dd/MM/yyyy")
        Call koneksi()
        Call refreshdata()
        Call addDGV()
        Timer1.Start()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click

    End Sub

    Private Sub ProgressBar1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProgressBar1.Click

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        ProgressBar1.Value += 1
        If ProgressBar1.Value = 2 Then
            Timer1.Dispose()
            MsgBox("Ada beberapa Kuli yang sudah kembali dari proyek HARI INI,klik OK untuk mengkondisikan ulang Kuli", MsgBoxStyle.Information)
            Call koneksi()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Hide()
        FormPosisi.Show()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Me.Hide()
        FormArsip.Show()
    End Sub

    Private Sub Button5_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Call koneksi()
        cmd = New OdbcCommand("select `order`.id_order,kuli.id_kuli,`order`.pengorder,kuli.nama_kuli,`order`.alamat_kerja,`order`.awal_kerja,`order`.akhir_kerja from order_detail inner join `order` on order_detail.id_order=`order`.id_order inner join kuli on order_detail.id_kuli=kuli.id_kuli where `order`.akhir_kerja = '" & Label29.Text & "'", conn)
        count = New OdbcCommand("select COUNT(*) from order_detail inner join `order` on order_detail.id_order=`order`.id_order inner join kuli on order_detail.id_kuli=kuli.id_kuli where `order`.akhir_kerja = '" & Label29.Text & "'", conn)
        Dim i As Integer = count.ExecuteScalar()
        rd = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            For value As Integer = 1 To i
                Call koneksi()
                Dim edit As String = "UPDATE kuli SET status ='Y' where id_kuli = '" & rd.Item("id_kuli") & "'"
                cmd = New OdbcCommand(edit, conn)
                cmd.ExecuteNonQuery()
                Call refreshdata()
            Next
        End If
        
        
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        With FormMenu
            .Show()
            Me.Close()
        End With
    End Sub
End Class