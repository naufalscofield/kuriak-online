﻿Imports System.Data.Odbc
Public Class FormOrder3
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    Sub koneksi()
        lokasiDB = "Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub
    Private Sub PictureBox6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox6.Click
        Call koneksi()
        Dim Pesan As String = MsgBox("Data Orderan Akan Ditambah?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Konfirmasi")
        If Pesan = vbYes Then
            Call koneksi()
            Dim simpan As String = "INSERT INTO `order` (pengorder,no_hp,email,alamat,alamat_kerja,jumlah_kuli,awal_kerja,akhir_kerja,input_dari,kode_pembayaran,lama_hari,atas_nama) values ('" & TextBox13.Text & "','" & TextBox14.Text & "','" & TextBox15.Text & "','" & TextBox16.Text & "','" & TextBox17.Text & "','" & TextBox3.Text & "','" & TextBox18.Text & "' , '" & TextBox19.Text & "', '" & TextBox5.Text & "', '" & TextBox7.Text & "','" & TextBox4.Text & "','" & TextBox9.Text & "')"
            cmd = New OdbcCommand(simpan, conn)
            cmd.ExecuteNonQuery()
            If CheckBox1.Checked = False Then
                TextBox9.Text = "-"
            Else
            End If
            MsgBox("data sudah disimpan", MsgBoxStyle.Information, "Kuriak Online")
            Me.Hide()
            FormMenu.Show()
        End If
    End Sub

    Private Sub PictureBox7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox7.Click
        FormOrder2.Show()
        Me.Close()
    End Sub

    Private Sub FormOrder3_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call koneksi()
        cmd = New OdbcCommand("select harga_kuli from setting", conn)
        rd = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows() Then
            TextBox10.Text = rd.Item("harga_kuli")
        End If
        Label1.Text = Val(TextBox10.Text) * Val(TextBox3.Text) * Val(TextBox4.Text) + DateTimePicker1.Value.Millisecond
        TextBox7.Text = DateTimePicker1.Value.Millisecond
    End Sub

    Private Sub TextBox3_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged

    End Sub

    Private Sub TextBox9_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox9.Click
        TextBox9.Text = ""
    End Sub

    Private Sub TextBox9_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox9.GotFocus

    End Sub

    Private Sub TextBox9_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox9.LostFocus

    End Sub

    Private Sub TextBox9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox9.TextChanged

    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            TextBox9.Visible = True
            TextBox9.Text = "Rekening A.N"
        Else
            TextBox9.Visible = False
            TextBox9.Text = "Cash On Place"
        End If
    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click

    End Sub

    Private Sub Label1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Label1.TextChanged
        Dim f As Long
        If Label1.Text = "----------" Or Not IsNumeric(Label1.Text) Then
            Exit Sub
        End If
        f = Label1.Text
        Label1.Text = Format(f, "##,##0")

    End Sub
End Class