﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_reg
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_reg))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.userimage = New System.Windows.Forms.PictureBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.pn = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.username = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.email = New System.Windows.Forms.TextBox()
        Me.password = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.nama = New System.Windows.Forms.TextBox()
        Me.btnCapture = New System.Windows.Forms.Button()
        Me.myimage = New System.Windows.Forms.PictureBox()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.btnRegister = New System.Windows.Forms.Button()
        Me.select_image = New System.Windows.Forms.Button()
        Me.btnRegistration = New System.Windows.Forms.Button()
        Me.btnRecords = New System.Windows.Forms.Button()
        Me.btnLogs = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txtmode = New System.Windows.Forms.TextBox()
        Me.txtaname = New System.Windows.Forms.TextBox()
        Me.userPass = New System.Windows.Forms.TextBox()
        Me.txtaccnt = New System.Windows.Forms.TextBox()
        Me.userID = New System.Windows.Forms.TextBox()
        Me.empID = New System.Windows.Forms.TextBox()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.ofdImage = New System.Windows.Forms.OpenFileDialog()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.userimage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.myimage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.userimage)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(645, 73)
        Me.Panel1.TabIndex = 0
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.ProjectTA.My.Resources.Resources.kuriak_logo
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(117, 79)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'userimage
        '
        Me.userimage.BackColor = System.Drawing.Color.MediumSlateBlue
        Me.userimage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.userimage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.userimage.Image = Global.ProjectTA.My.Resources.Resources.noimg
        Me.userimage.Location = New System.Drawing.Point(584, 9)
        Me.userimage.Name = "userimage"
        Me.userimage.Size = New System.Drawing.Size(48, 42)
        Me.userimage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.userimage.TabIndex = 35
        Me.userimage.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.pn)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.username)
        Me.GroupBox1.Controls.Add(Me.Label27)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.email)
        Me.GroupBox1.Controls.Add(Me.password)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.nama)
        Me.GroupBox1.ForeColor = System.Drawing.Color.Black
        Me.GroupBox1.Location = New System.Drawing.Point(185, 85)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(447, 195)
        Me.GroupBox1.TabIndex = 139
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Bio"
        '
        'pn
        '
        Me.pn.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pn.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.pn.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pn.Location = New System.Drawing.Point(84, 137)
        Me.pn.Name = "pn"
        Me.pn.Size = New System.Drawing.Size(145, 20)
        Me.pn.TabIndex = 6
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(2, 141)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(78, 13)
        Me.Label16.TabIndex = 154
        Me.Label16.Text = "Phone Number"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(1, 43)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(58, 13)
        Me.Label6.TabIndex = 150
        Me.Label6.Text = "Username:"
        '
        'username
        '
        Me.username.BackColor = System.Drawing.Color.WhiteSmoke
        Me.username.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.username.Location = New System.Drawing.Point(84, 39)
        Me.username.Name = "username"
        Me.username.Size = New System.Drawing.Size(98, 20)
        Me.username.TabIndex = 2
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.BackColor = System.Drawing.Color.Transparent
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.Black
        Me.Label27.Location = New System.Drawing.Point(2, 117)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(39, 13)
        Me.Label27.TabIndex = 148
        Me.Label27.Text = "E-Mail:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(1, 68)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 13)
        Me.Label1.TabIndex = 136
        Me.Label1.Text = "Password:"
        '
        'email
        '
        Me.email.BackColor = System.Drawing.Color.WhiteSmoke
        Me.email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.email.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.email.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.email.Location = New System.Drawing.Point(84, 112)
        Me.email.Name = "email"
        Me.email.Size = New System.Drawing.Size(145, 20)
        Me.email.TabIndex = 5
        '
        'password
        '
        Me.password.BackColor = System.Drawing.Color.WhiteSmoke
        Me.password.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.password.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.password.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.password.Location = New System.Drawing.Point(84, 65)
        Me.password.Name = "password"
        Me.password.Size = New System.Drawing.Size(100, 20)
        Me.password.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(2, 92)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 13)
        Me.Label3.TabIndex = 140
        Me.Label3.Text = "Full Name:"
        '
        'nama
        '
        Me.nama.BackColor = System.Drawing.Color.WhiteSmoke
        Me.nama.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nama.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.nama.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.nama.Location = New System.Drawing.Point(84, 89)
        Me.nama.Name = "nama"
        Me.nama.Size = New System.Drawing.Size(351, 20)
        Me.nama.TabIndex = 4
        '
        'btnCapture
        '
        Me.btnCapture.BackColor = System.Drawing.Color.DimGray
        Me.btnCapture.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCapture.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnCapture.Image = CType(resources.GetObject("btnCapture.Image"), System.Drawing.Image)
        Me.btnCapture.Location = New System.Drawing.Point(95, 226)
        Me.btnCapture.Name = "btnCapture"
        Me.btnCapture.Size = New System.Drawing.Size(82, 25)
        Me.btnCapture.TabIndex = 8
        Me.btnCapture.Text = "Camera"
        Me.btnCapture.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCapture.UseCompatibleTextRendering = True
        Me.btnCapture.UseVisualStyleBackColor = False
        '
        'myimage
        '
        Me.myimage.BackColor = System.Drawing.Color.MediumSlateBlue
        Me.myimage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.myimage.Image = Global.ProjectTA.My.Resources.Resources.noimg
        Me.myimage.Location = New System.Drawing.Point(25, 85)
        Me.myimage.Name = "myimage"
        Me.myimage.Size = New System.Drawing.Size(132, 131)
        Me.myimage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.myimage.TabIndex = 134
        Me.myimage.TabStop = False
        '
        'btnReset
        '
        Me.btnReset.BackColor = System.Drawing.Color.DimGray
        Me.btnReset.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnReset.Image = CType(resources.GetObject("btnReset.Image"), System.Drawing.Image)
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReset.Location = New System.Drawing.Point(95, 257)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(82, 23)
        Me.btnReset.TabIndex = 10
        Me.btnReset.Text = "Reset"
        Me.btnReset.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnReset.UseCompatibleTextRendering = True
        Me.btnReset.UseVisualStyleBackColor = False
        '
        'btnRegister
        '
        Me.btnRegister.BackColor = System.Drawing.Color.DimGray
        Me.btnRegister.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnRegister.Image = CType(resources.GetObject("btnRegister.Image"), System.Drawing.Image)
        Me.btnRegister.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRegister.Location = New System.Drawing.Point(12, 257)
        Me.btnRegister.Name = "btnRegister"
        Me.btnRegister.Size = New System.Drawing.Size(84, 23)
        Me.btnRegister.TabIndex = 9
        Me.btnRegister.Text = "Register"
        Me.btnRegister.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnRegister.UseCompatibleTextRendering = True
        Me.btnRegister.UseVisualStyleBackColor = False
        '
        'select_image
        '
        Me.select_image.BackColor = System.Drawing.Color.DimGray
        Me.select_image.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.select_image.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.select_image.Image = CType(resources.GetObject("select_image.Image"), System.Drawing.Image)
        Me.select_image.Location = New System.Drawing.Point(12, 226)
        Me.select_image.Name = "select_image"
        Me.select_image.Size = New System.Drawing.Size(84, 25)
        Me.select_image.TabIndex = 7
        Me.select_image.Text = "Select"
        Me.select_image.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.select_image.UseCompatibleTextRendering = True
        Me.select_image.UseVisualStyleBackColor = False
        '
        'btnRegistration
        '
        Me.btnRegistration.BackColor = System.Drawing.Color.Gold
        Me.btnRegistration.FlatAppearance.BorderColor = System.Drawing.Color.Teal
        Me.btnRegistration.FlatAppearance.BorderSize = 0
        Me.btnRegistration.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnRegistration.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.btnRegistration.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRegistration.ForeColor = System.Drawing.Color.Black
        Me.btnRegistration.Image = CType(resources.GetObject("btnRegistration.Image"), System.Drawing.Image)
        Me.btnRegistration.Location = New System.Drawing.Point(183, 5)
        Me.btnRegistration.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnRegistration.Name = "btnRegistration"
        Me.btnRegistration.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnRegistration.Size = New System.Drawing.Size(65, 56)
        Me.btnRegistration.TabIndex = 19
        Me.btnRegistration.Text = "REGISTER"
        Me.btnRegistration.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnRegistration.UseVisualStyleBackColor = False
        '
        'btnRecords
        '
        Me.btnRecords.BackColor = System.Drawing.Color.Tomato
        Me.btnRecords.FlatAppearance.BorderColor = System.Drawing.Color.Teal
        Me.btnRecords.FlatAppearance.BorderSize = 0
        Me.btnRecords.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnRecords.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.btnRecords.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRecords.ForeColor = System.Drawing.Color.Black
        Me.btnRecords.Image = CType(resources.GetObject("btnRecords.Image"), System.Drawing.Image)
        Me.btnRecords.Location = New System.Drawing.Point(249, 5)
        Me.btnRecords.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnRecords.Name = "btnRecords"
        Me.btnRecords.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnRecords.Size = New System.Drawing.Size(65, 56)
        Me.btnRecords.TabIndex = 20
        Me.btnRecords.Text = "RECORDS"
        Me.btnRecords.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnRecords.UseVisualStyleBackColor = False
        '
        'btnLogs
        '
        Me.btnLogs.BackColor = System.Drawing.Color.SteelBlue
        Me.btnLogs.FlatAppearance.BorderColor = System.Drawing.Color.Teal
        Me.btnLogs.FlatAppearance.BorderSize = 0
        Me.btnLogs.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btnLogs.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.btnLogs.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogs.ForeColor = System.Drawing.Color.Black
        Me.btnLogs.Image = CType(resources.GetObject("btnLogs.Image"), System.Drawing.Image)
        Me.btnLogs.Location = New System.Drawing.Point(315, 6)
        Me.btnLogs.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.btnLogs.Name = "btnLogs"
        Me.btnLogs.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnLogs.Size = New System.Drawing.Size(65, 56)
        Me.btnLogs.TabIndex = 23
        Me.btnLogs.Text = "LOGS"
        Me.btnLogs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnLogs.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Panel2.Controls.Add(Me.PictureBox2)
        Me.Panel2.Controls.Add(Me.Button1)
        Me.Panel2.Controls.Add(Me.txtmode)
        Me.Panel2.Controls.Add(Me.txtaname)
        Me.Panel2.Controls.Add(Me.userPass)
        Me.Panel2.Controls.Add(Me.txtaccnt)
        Me.Panel2.Controls.Add(Me.userID)
        Me.Panel2.Controls.Add(Me.empID)
        Me.Panel2.Controls.Add(Me.btnRegistration)
        Me.Panel2.Controls.Add(Me.btnRecords)
        Me.Panel2.Controls.Add(Me.btnLogs)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 302)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(644, 64)
        Me.Panel2.TabIndex = 140
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.MediumOrchid
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.Teal
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Image = Global.ProjectTA.My.Resources.Resources.chart_32
        Me.Button1.Location = New System.Drawing.Point(381, 6)
        Me.Button1.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Button1.Size = New System.Drawing.Size(65, 56)
        Me.Button1.TabIndex = 147
        Me.Button1.Text = "CHART"
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Button1.UseVisualStyleBackColor = False
        '
        'txtmode
        '
        Me.txtmode.BackColor = System.Drawing.Color.Gray
        Me.txtmode.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtmode.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtmode.Location = New System.Drawing.Point(622, 5)
        Me.txtmode.Name = "txtmode"
        Me.txtmode.Size = New System.Drawing.Size(10, 20)
        Me.txtmode.TabIndex = 60
        Me.txtmode.Visible = False
        '
        'txtaname
        '
        Me.txtaname.BackColor = System.Drawing.Color.Gray
        Me.txtaname.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtaname.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtaname.Location = New System.Drawing.Point(611, 5)
        Me.txtaname.Name = "txtaname"
        Me.txtaname.Size = New System.Drawing.Size(10, 20)
        Me.txtaname.TabIndex = 59
        Me.txtaname.Visible = False
        '
        'userPass
        '
        Me.userPass.BackColor = System.Drawing.Color.Gray
        Me.userPass.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.userPass.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.userPass.Location = New System.Drawing.Point(600, 5)
        Me.userPass.Name = "userPass"
        Me.userPass.Size = New System.Drawing.Size(10, 20)
        Me.userPass.TabIndex = 39
        Me.userPass.Visible = False
        '
        'txtaccnt
        '
        Me.txtaccnt.BackColor = System.Drawing.Color.Gray
        Me.txtaccnt.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.txtaccnt.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtaccnt.Location = New System.Drawing.Point(590, 5)
        Me.txtaccnt.Name = "txtaccnt"
        Me.txtaccnt.Size = New System.Drawing.Size(10, 20)
        Me.txtaccnt.TabIndex = 38
        Me.txtaccnt.Visible = False
        '
        'userID
        '
        Me.userID.BackColor = System.Drawing.Color.Gray
        Me.userID.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.userID.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.userID.Location = New System.Drawing.Point(580, 5)
        Me.userID.Name = "userID"
        Me.userID.Size = New System.Drawing.Size(10, 20)
        Me.userID.TabIndex = 37
        Me.userID.Visible = False
        '
        'empID
        '
        Me.empID.BackColor = System.Drawing.Color.Gray
        Me.empID.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.empID.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.empID.Location = New System.Drawing.Point(570, 5)
        Me.empID.Name = "empID"
        Me.empID.Size = New System.Drawing.Size(10, 20)
        Me.empID.TabIndex = 36
        Me.empID.Visible = False
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(9, 197)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(10, 17)
        Me.ListBox1.TabIndex = 145
        Me.ListBox1.Visible = False
        '
        'ofdImage
        '
        Me.ofdImage.FileName = "OpenFileDialog1"
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(9, 14)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(42, 38)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 56
        Me.PictureBox2.TabStop = False
        '
        'Form_reg
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ProjectTA.My.Resources.Resources.KURIAK_BG
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(644, 366)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnCapture)
        Me.Controls.Add(Me.myimage)
        Me.Controls.Add(Me.btnReset)
        Me.Controls.Add(Me.btnRegister)
        Me.Controls.Add(Me.select_image)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form_reg"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form_reg"
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.userimage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.myimage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents userimage As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents pn As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents username As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents email As System.Windows.Forms.TextBox
    Friend WithEvents password As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents nama As System.Windows.Forms.TextBox
    Friend WithEvents btnCapture As System.Windows.Forms.Button
    Friend WithEvents myimage As System.Windows.Forms.PictureBox
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents btnRegister As System.Windows.Forms.Button
    Friend WithEvents select_image As System.Windows.Forms.Button
    Friend WithEvents btnRegistration As System.Windows.Forms.Button
    Friend WithEvents btnRecords As System.Windows.Forms.Button
    Friend WithEvents btnLogs As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtmode As System.Windows.Forms.TextBox
    Friend WithEvents txtaname As System.Windows.Forms.TextBox
    Friend WithEvents userPass As System.Windows.Forms.TextBox
    Friend WithEvents txtaccnt As System.Windows.Forms.TextBox
    Friend WithEvents userID As System.Windows.Forms.TextBox
    Friend WithEvents empID As System.Windows.Forms.TextBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents ofdImage As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
End Class
