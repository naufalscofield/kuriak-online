﻿Public Class FormLoading
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        ProgressBar1.Value += 1
        If ProgressBar1.Value = 32 Then
            Timer1.Dispose()
            Me.Visible = False
            FormLogin.Show()
        End If
        If ProgressBar1.Value = 1 Then
            titik1.Visible = True
        End If
        If ProgressBar1.Value = 6 Then
            titik2.Visible = True
        End If
        If ProgressBar1.Value = 12 Then
            titik3.Visible = True
        End If
        If ProgressBar1.Value = 18 Then
            titik1.Visible = False
            titik2.Visible = False
            titik3.Visible = False
        End If
        If ProgressBar1.Value = 24 Then
            titik1.Visible = True
        End If
        If ProgressBar1.Value = 28 Then
            titik2.Visible = True
        End If
        If ProgressBar1.Value = 32 Then
            titik3.Visible = True
        End If
    End Sub

    Private Sub FormLoading_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Timer1.Start()
    End Sub
End Class
