﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports System.Data.Odbc
Public Class FormExcel
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    Sub koneksi()
        lokasiDB = "Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub
    Sub refreshdata()
        Call koneksi()
        da = New OdbcDataAdapter("select * from `order`", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "`order`")
        dgv.DataSource = (ds.Tables("`order`"))
    End Sub

    Private Sub FormExcel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call refreshdata()
    End Sub
    Public Function funcDataGridViewToExcel(ByVal pDGView As DataGridView, ByVal pPathFile As String) As Boolean
        Try
            If pDGView.RowCount = 0 Then Return False
            Dim varExcelApp As Excel.ApplicationClass
            Dim varExcelWorkBook As Excel.Workbook
            Dim varExcelWorkSheet As Excel.Worksheet
            Dim misValue As Object = System.Reflection.Missing.Value

            varExcelApp = New Excel.ApplicationClass
            varExcelWorkBook = varExcelApp.Workbooks.Add(misValue)
            varExcelWorkSheet = varExcelWorkBook.Sheets("sheet1")
            'add header
            For i As Integer = 0 To pDGView.ColumnCount - 1
                varExcelWorkSheet.Cells(1, i + 1) = pDGView.Columns(i).HeaderText
            Next
            'add data
            For i As Integer = 0 To pDGView.RowCount - 1
                For j As Integer = 0 To pDGView.ColumnCount - 1
                    varExcelWorkSheet.Cells(i + 2, j + 1) = IIf(IsDBNull(pDGView.Rows(i).Cells(j).Value.ToString()) = True, "", pDGView.Rows(i).Cells(j).Value.ToString())
                Next
            Next

            varExcelWorkSheet.SaveAs(pPathFile)
            varExcelWorkBook.Close()
            varExcelApp.Quit()

            releaseObject(varExcelApp)
            releaseObject(varExcelWorkBook)
            releaseObject(varExcelWorkSheet)
            Return True
        Catch ex As Exception
            MessageBox.Show(ex.Message & vbCrLf & ex.Source, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try
    End Function
    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        PictureBox1.Visible = True
        Label2.Visible = True
        ProgressBar1.Visible = True
        Panel2.Visible = True
        Timer1.Start()
        Label1.Visible = False
        Button3.Visible = False
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        ProgressBar1.Value += 1
        If ProgressBar1.Value = 100 Then
            Timer1.Dispose()
            Dim xlApp As Excel.Application
            Dim xlWorkBook As Excel.Workbook
            Dim xlWorkSheet As Excel.Worksheet
            Dim misValue As Object = System.Reflection.Missing.Value
            Dim i As Integer
            Dim j As Integer

            xlApp = New Excel.ApplicationClass
            xlWorkBook = xlApp.Workbooks.Add(misValue)
            xlWorkSheet = xlWorkBook.Sheets("sheet1")

            For i = 0 To dgv.RowCount - 2
                For j = 0 To dgv.ColumnCount - 1
                    xlWorkSheet.Cells(i + 1, j + 1) = _
                        dgv(j, i).Value.ToString()
                Next
            Next

            xlWorkSheet.SaveAs("E:\OrderanKuriakOnline.xlsx")
            xlWorkBook.Close()
            xlApp.Quit()


            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)

            MsgBox("Anda dapat menemukan file .xlsx nya di >> E:\OrderanKuriakOnline.xlsx")
            Me.Hide()
            FormMenu.Show()
        End If
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        FormMenu.Show()
        Me.Close()
    End Sub
End Class