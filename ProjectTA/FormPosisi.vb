﻿Imports System.Data.Odbc
Public Class FormPosisi
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    Sub koneksi()
        lokasiDB = "Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub
    Public Sub addDGV()

        With dgv
            'menghilangkan row header
            .RowHeadersVisible = False
            'menghilangkan colum header
            .ColumnHeadersVisible = False
            'agar ketika baris di datagrid diklik 1x melalui mouse atau kursor, satu baris penuh yang pilih, bukan per kolom
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Columns(0).Width = 56
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 70
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).Width = 126
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).Width = 144
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).Width = 236
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            


        End With
    End Sub

    Sub refreshdata()
        Call koneksi()
        da = New OdbcDataAdapter("select `order`.id_order,kuli.id_kuli,`order`.pengorder,kuli.nama_kuli,`order`.alamat_kerja from order_detail inner join `order` on order_detail.id_order=`order`.id_order inner join kuli on order_detail.id_kuli=kuli.id_kuli WHERE `order`.akhir_kerja < '" & Format(Now, "dd/MM/yyyy") & "' and kuli.status ='N'", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "order_detail")
        dgv.DataSource = (ds.Tables("order_detail"))
    End Sub

    Private Sub FormPosisi_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call refreshdata()
        Call addDGV()
    End Sub

    Private Sub TextBox9_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox9.GotFocus
        TextBox9.Text = ""
    End Sub

    Private Sub TextBox9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox9.KeyPress
        Dim strValid As String = "0123456789-.,"

        Dim x As Long = InStr(strValid, e.KeyChar)

        If x = 0 And Asc(e.KeyChar) <> 8 And Asc(e.KeyChar) <> 32 Then

            e.KeyChar = ""

        End If
    End Sub

    Private Sub TextBox9_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox9.LostFocus
        TextBox9.Text = "Temukan"
    End Sub

    Private Sub TextBox9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox9.TextChanged
        Call koneksi()
        cmd = New OdbcCommand("select `order`.id_order,kuli.id_kuli,`order`.pengorder,kuli.nama_kuli,`order`.alamat_kerja from order_detail inner join `order` on order_detail.id_order=`order`.id_order inner join kuli on order_detail.id_kuli=kuli.id_kuli where kuli.id_kuli like'%" & TextBox9.Text & "%'", conn)
        rd = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            da = New OdbcDataAdapter("select `order`.id_order,kuli.id_kuli,`order`.pengorder,kuli.nama_kuli,`order`.alamat_kerja from order_detail inner join `order` on order_detail.id_order=`order`.id_order inner join kuli on order_detail.id_kuli=kuli.id_kuli where kuli.id_kuli like'%" & TextBox9.Text & "%'", conn)
            ds = New DataSet
            ds.Clear()
            da.Fill(ds, "order_detail")
            dgv.DataSource = ds.Tables("order_detail")
        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Me.Hide()
        FormArsip.Show()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Me.Hide()
        FormDone.Show()
    End Sub
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Hide()
        FormOrderan.Show()
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        With FormMenu
            .Show()
            Me.Close()
        End With
    End Sub
End Class