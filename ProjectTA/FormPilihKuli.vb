﻿Imports System.Data.Odbc
Public Class FormPilihKuli
    Dim conn As New OdbcConnection
    Dim cmd As New OdbcCommand
    Dim da As New OdbcDataAdapter
    Dim ds As New DataSet
    Dim rd As OdbcDataReader
    Dim lokasiDB As String
    Sub koneksi()
        lokasiDB = "Dsn=DSNKuriak;server=localhost;uid=root;database=kuriak;port=3306"
        conn = New OdbcConnection(lokasiDB)
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
    End Sub
    


    Sub refreshdata()
        Call koneksi()
        da = New OdbcDataAdapter("select * from `kuli` where status = 'Y' ", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "`kuli`")
        dgv.DataSource = (ds.Tables("`kuli`"))
    End Sub
    Sub refreshdata2()
        Call koneksi()
        da = New OdbcDataAdapter("select * from `kuli` where status = 'Y' ", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "`kuli`")
        dgv.DataSource = (ds.Tables("`kuli`"))
        If Label2.Text = "0" Then
            Call refreshdata()
            MsgBox("Kuli Dipekerjakan")
            Me.Hide()
            Formprint.Show()
            Formprint.ComboBox1.Text = Me.TextBox4.Text
        End If
    End Sub
    Sub hitung()
        Dim Bil1, Bil2 As Integer

        Dim Hasil As Long



        Bil1 = Val(Label5.Text)

        Bil2 = Val(Label4.Text)

        Hasil = Val(Label2.Text)



        Hasil = (Bil1 - Bil2)

        Label2.Text = Hasil
        Label5.Text = Hasil
    End Sub
    Public Sub addDGV()

        With dgv
            'menghilangkan row header
            .RowHeadersVisible = False
            'menghilangkan colum header
            .ColumnHeadersVisible = False
            'agar ketika baris di datagrid diklik 1x melalui mouse atau kursor, satu baris penuh yang pilih, bukan per kolom
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .Columns(0).Width = 30
            .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(1).Width = 139
            .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(2).Width = 205
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(3).Width = 211
            .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns(4).Width = 78
            .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            



        End With
    End Sub

    Private Sub FormPilihKuli_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        TextBox9.Focus()
        Label2.Text = FormOrderan.TextBox16.Text
        Label5.Text = FormOrderan.TextBox16.Text
        TextBox4.Text = FormOrderan.TextBox15.Text
        Call refreshdata()
        Call addDGV()
    End Sub
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If TextBox1.Text = "" Then
            MsgBox("Pilih Kuli!", MsgBoxStyle.Information)
        Else
            Call koneksi()
            Dim edit As String = "UPDATE kuli SET status ='" & TextBox2.Text & "' where id_kuli = '" & TextBox1.Text & "'"
            cmd = New OdbcCommand(edit, conn)
            cmd.ExecuteNonQuery()
            Dim simpan As String = "INSERT INTO order_detail (id_order,id_kuli) values ('" & TextBox4.Text & "','" & TextBox1.Text & "')"
            cmd = New OdbcCommand(simpan, conn)
            cmd.ExecuteNonQuery()
            Call hitung()
            Call refreshdata2()
        End If
    End Sub


    Private Sub dgv_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellContentClick
        Button4.Visible = True
        Dim a As Object = dgv.Rows(e.RowIndex).Cells(0).Value
        TextBox1.Text = a
    End Sub

    Private Sub TextBox9_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox9.GotFocus
        TextBox9.Text = ""
    End Sub

    Private Sub TextBox9_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox9.LostFocus
        TextBox9.Text = "Temukan"
    End Sub

    Private Sub TextBox9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox9.TextChanged
        Call koneksi()
        cmd = New OdbcCommand("select * from kuli where kecamatan like'%" & TextBox9.Text & "%'", conn)
        rd = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            da = New OdbcDataAdapter("select * from kuli where kecamatan like '%" & TextBox9.Text & "%'", conn)
            ds = New DataSet
            ds.Clear()
            da.Fill(ds, "kuli")
            dgv.DataSource = ds.Tables("kuli")
        End If
    End Sub
End Class