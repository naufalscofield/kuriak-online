-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 26 Jan 2016 pada 05.48
-- Versi Server: 5.5.27
-- Versi PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `kuriak`
--
CREATE DATABASE `kuriak` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `kuriak`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `id_gallery` int(11) NOT NULL AUTO_INCREMENT,
  `tittle` varchar(15) NOT NULL,
  `content` varchar(30) NOT NULL,
  `gambar` longblob NOT NULL,
  PRIMARY KEY (`id_gallery`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kuli`
--

CREATE TABLE IF NOT EXISTS `kuli` (
  `id_kuli` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kuli` varchar(100) NOT NULL,
  `kecamatan` int(100) NOT NULL,
  `no_telp` int(20) NOT NULL,
  `status` enum('Y','N') NOT NULL,
  PRIMARY KEY (`id_kuli`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `kuli`
--

INSERT INTO `kuli` (`id_kuli`, `nama_kuli`, `kecamatan`, `no_telp`, `status`) VALUES
(1, 'Aldi', 0, 0, 'Y'),
(2, 'Naufal', 0, 0, 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `pengorder` varchar(50) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `alamat_kerja` text NOT NULL,
  `jumlah_kuli` int(5) NOT NULL,
  `awal_kerja` date NOT NULL,
  `akhir_kerja` date NOT NULL,
  `input_dari` varchar(10) NOT NULL,
  `status` enum('S','BS') NOT NULL DEFAULT 'BS',
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `order`
--

INSERT INTO `order` (`id_order`, `pengorder`, `no_hp`, `email`, `alamat`, `alamat_kerja`, `jumlah_kuli`, `awal_kerja`, `akhir_kerja`, `input_dari`, `status`) VALUES
(1, 'amin', '08987897', 'si.logoz@gmail.com', 'Jlanncasnkcsnkacn', 'sad,lasmdlasml', 11, '0000-00-00', '0000-00-00', 'web', 'BS');

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_detail`
--

CREATE TABLE IF NOT EXISTS `order_detail` (
  `id_order` int(11) NOT NULL,
  `id_kuli` int(11) NOT NULL,
  PRIMARY KEY (`id_order`,`id_kuli`),
  KEY `id_kuli` (`id_kuli`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `id_setting` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(100) NOT NULL,
  `company_phone` varchar(20) NOT NULL,
  `company_email` varchar(100) NOT NULL,
  `company_address` varchar(100) NOT NULL,
  `meta_description` varchar(100) NOT NULL,
  `meta_keyword` varchar(100) NOT NULL,
  `favicon` longblob NOT NULL,
  `company_maintenance` enum('Y','N') NOT NULL,
  PRIMARY KEY (`id_setting`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `setting`
--

INSERT INTO `setting` (`id_setting`, `company_name`, `company_phone`, `company_email`, `company_address`, `meta_description`, `meta_keyword`, `favicon`, `company_maintenance`) VALUES
(1, 'Kuriak Online', '022-723-7306', 'kuriak@kuribray.com', 'Rumah Aldi', 'Kuriak Online adalah situs Rental Kuli', 'Kuriak, Kuli, Bangun Rumah, Tukang Bangunan', '', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `level` enum('1','2') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `order_detail`
--
ALTER TABLE `order_detail`
  ADD CONSTRAINT `order_detail_ibfk_1` FOREIGN KEY (`id_order`) REFERENCES `order` (`id_order`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `order_detail_ibfk_2` FOREIGN KEY (`id_kuli`) REFERENCES `kuli` (`id_kuli`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
